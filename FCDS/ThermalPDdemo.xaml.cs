﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.ML.Structure;
using Emgu.CV.Structure;
using FCDS.Classes;
using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Collections.Generic;
using System.IO;

namespace FCDS
{
    public partial class ThermalPDdemo : Window
    {
        // Declaration of global variables
        int frameNumber;
        int lowThreshold, highThreshold;
        int predictionThreadCount;

        double classifierThreshold;
        int detectorWidth, detectorHeight;

        // Declaration of main objects
        FeatureExtractor usedFeatureExtractor;
        Classifier usedClassifier;
        TDataset usedDataset;
        PedestrianDetectionModule detectionModule;
        FiltrationParameters segmentationFiltrationParams;
        TSegmentator segmentionFunction;
        string pathToCNNmodelWeights;
        
        // Application initialization
        public ThermalPDdemo()
        {
            // Setting initial values for variables
            frameNumber = 0;
            InitializeComponent();

            // Thresholding method. 1 = single thresholding, 2 - double thresholding, 3 - triple thresholding
            segmentationFiltrationParams.thresholdingMethod = 2;

            // Important parameters of segmentation process
            segmentationFiltrationParams.homogenousRegionsCoef = 20;
            segmentationFiltrationParams.minArea = 150;
            segmentationFiltrationParams.minHeightCoef = 0.35f;
            segmentationFiltrationParams.minRatio = 0.8f;
            segmentationFiltrationParams.maxRatio = 7f;
            segmentationFiltrationParams.similatiry = 0.8f;
            segmentationFiltrationParams.skewnees = 0.15f;
            segmentationFiltrationParams.fulfilment = 0.2f;
            segmentationFiltrationParams.minAreaInit = 12;
            segmentationFiltrationParams.maxROIcount = 100;
            segmentationFiltrationParams.filtration = true;
            segmentationFiltrationParams.regionEnlargement = true;
            segmentationFiltrationParams.morfological = true;
            segmentationFiltrationParams.initialBlobSelection = true;
        }

        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);

        private void Load_CNN_detector_Button_Click(object sender, RoutedEventArgs e)
        {
            updateApplicationAndValues();

            selectCNNdetector(detectorWidth, detectorHeight);
        }

        private void Train_ACF_detector_Button_Click(object sender, RoutedEventArgs e)
        {
            updateApplicationAndValues();

            selectACFdetector(detectorWidth, detectorHeight);
        }
        private void updateApplicationAndValues()
        {
            lowerTHtextBox.IsEnabled = true;
            higherTHtextBox.IsEnabled = true;
            backButton.IsEnabled = true;
            forwardButton.IsEnabled = true;
            alfaTHtextBox.IsEnabled = true;
            betaTHtextBox.IsEnabled = true;
            classTHtextBox.IsEnabled = true;

            detectButton.IsEnabled = true;
            classificationButton.IsEnabled = true;

            detectorWidth = Int16.Parse(detectorWidthTextBox.Text);
            detectorHeight = Int16.Parse(detectorHeightTextBox.Text);

            // Set Path to dataset of recordings. Each dataset need to have own class to handle data format
            String datasetPath = CVC14pathTextBox.Text;
            usedDataset = new CVC14_TDataset(datasetPath);

            // set path to the trained CNN model - .h5 file, model structure need to be the same as in CNNFeatureExtractor class constructor.
            pathToCNNmodelWeights = CNNpathTextBox.Text;

            // Set thresholding method
            predictionThreadCount = 8;
            if (manualThresholdsRadioButton.IsChecked == true)
                segmentionFunction = new Simple_TSegmentator(segmentationFiltrationParams);
            else
                segmentionFunction = new OTSU_TSegmentator(segmentationFiltrationParams);
        }

        private void Detect_pedestrians_Button_Click(object sender, RoutedEventArgs e)
        {
            detectPedestrians();
        }        
        private void detectPedestrians()
        {
            Image<Bgr, Byte>[] resultingImagesArray;

            setDetectionParameters();

            // perform detection process
            resultingImagesArray = detectionModule.detectionTestsDualThresh(frameNumber, true, lowThreshold, highThreshold, classifierThreshold);

            // show results in appliaction
            drawImages(resultingImagesArray);
        }
        private void setDetectionParameters()
        {
            // get detection parameters
            lowThreshold = Int32.Parse(lowerTHtextBox.Text);
            highThreshold = Int32.Parse(higherTHtextBox.Text);
            classifierThreshold = double.Parse(classTHtextBox.Text);

            // set thresholding method and segmentation parameters
            // Set thresholding method
            if (manualThresholdsRadioButton.IsChecked == true)
            {
                lowThreshold = Int32.Parse(lowerTHtextBox.Text);
                highThreshold = Int32.Parse(higherTHtextBox.Text);
                segmentionFunction = new Simple_TSegmentator(segmentationFiltrationParams);
                detectionModule.updateSegmentator(segmentionFunction);
            }
            else
            {
                int alfa = Int32.Parse(alfaTHtextBox.Text);
                int beta = Int32.Parse(betaTHtextBox.Text);
                lowThreshold = alfa - beta;
                highThreshold = alfa + beta;
                segmentionFunction = new OTSU_TSegmentator(segmentationFiltrationParams);
            }
            detectionModule.updateSegmentator(segmentionFunction);

            // 1 - single thresholding, 2 - double thresholding, 3 - triple thresholding
            segmentionFunction.setThresholdingMethod(2);
            detectionModule.setOptimalBestParametersDoubleThresholding();
        }

        private void Scroll_Forward_Button_Click(object sender, RoutedEventArgs e)
        {
            frameNumber++;

            detectPedestrians();            
        }

        private void Scroll_Back_Button_Click(object sender, RoutedEventArgs e)
        {
            frameNumber--;

            detectPedestrians();
        }

        private void Classification_tests_Button_Click(object sender, RoutedEventArgs e)
        {
            setDetectionParameters();

            float[] statistics = detectionModule.fullScaleDetectionTest(lowThreshold, highThreshold, classifierThreshold);

            missRateTextBox.Text = statistics[0].ToString();
            fpsTextBox.Text = statistics[1].ToString();
            timeTextBox.Text = statistics[2].ToString();
            samplesTextBox.Text = statistics[3].ToString();
        }

        // Minor fucntions
        private void selectACFdetector(int x, int y)
        {
            // Declatation of used feature extractor 
            usedFeatureExtractor = new ACF_FeatureExtractor(new System.Drawing.Size(x, y)
                                                                   , new System.Drawing.Size(16, 16)
                                                                   , new System.Drawing.Size(8, 8)
                                                                   , new System.Drawing.Size(8, 8)
                                                                   , 9);
            // AdaBoost classifier parameters
            MCvBoostParams boostParameters = new MCvBoostParams();
            boostParameters.boostType = Emgu.CV.ML.MlEnum.BOOST_TYPE.DISCRETE;
            boostParameters.weakCount = 2048;
            boostParameters.weightTrimRate = 0.95; // Note : to turn off set to 0
            boostParameters.maxDepth = 2;
            boostParameters.useSurrogates = false;
            boostParameters.maxCategories = 2;
            boostParameters.minSampleCount = 220;
            
            if (usedClassifier != null) usedClassifier.clearClassifier();
            usedClassifier = new BOOST_Classifier(boostParameters);

            // Declatation of main object that perfrom pedestrian detection
            detectionModule = new PedestrianDetectionModule(usedDataset, usedFeatureExtractor, usedClassifier, segmentionFunction, predictionThreadCount);

            // Prepare database for test, train the classifier or read from file  
            detectionModule.initialization();
        }
        private void selectHOG_ADAdetector(int x, int y)
        {
            // Declatation of used feature extractor 
            usedFeatureExtractor = new HOGFeatureExtractor(new System.Drawing.Size(x, y)
                                                                   , new System.Drawing.Size(16, 16)
                                                                   , new System.Drawing.Size(8, 8)
                                                                   , new System.Drawing.Size(8, 8)
                                                                   , 6);
            // AdaBoost classifier parameters
            MCvBoostParams boostParameters = new MCvBoostParams();
            boostParameters.boostType = Emgu.CV.ML.MlEnum.BOOST_TYPE.REAL;
            boostParameters.weakCount = 2048;
            boostParameters.weightTrimRate = 0.95; // Note : to turn off set to 0
            boostParameters.maxDepth = 2;
            boostParameters.useSurrogates = false;
            boostParameters.maxCategories = 2;
            boostParameters.minSampleCount = 220;


            if (usedClassifier != null) usedClassifier.clearClassifier();
            usedClassifier = new BOOST_Classifier(boostParameters);

            // Declatation of main object that perfrom pedestrian detection
            detectionModule = new PedestrianDetectionModule(usedDataset, usedFeatureExtractor, usedClassifier, segmentionFunction, predictionThreadCount);

            // Prepare database for test, train the classifier or read from file  
            detectionModule.initialization();
        }
        private void selectHOG_SVMdetector(int x, int y)
        {
            // Declatation of used feature extractor 
            usedFeatureExtractor = new HOGFeatureExtractor(new System.Drawing.Size(x, y)
                                                                   , new System.Drawing.Size(16, 16)
                                                                   , new System.Drawing.Size(8, 8)
                                                                   , new System.Drawing.Size(8, 8)
                                                                   , 6);
            // SVM classifier parameters
            SVMParams svmEmguParams = new SVMParams();
            svmEmguParams.KernelType = Emgu.CV.ML.MlEnum.SVM_KERNEL_TYPE.LINEAR;
            svmEmguParams.SVMType = Emgu.CV.ML.MlEnum.SVM_TYPE.C_SVC;
            svmEmguParams.C = 1;
            svmEmguParams.TermCrit = new MCvTermCriteria(100, 0.00001);

            if (usedClassifier != null) usedClassifier.clearClassifier();
            usedClassifier = new LIBSVM_Classifier();

            // Declatation of main object that perfrom pedestrian detection
            detectionModule = new PedestrianDetectionModule(usedDataset, usedFeatureExtractor, usedClassifier, segmentionFunction, predictionThreadCount);

            // Prepare database for test, train the classifier or read from file  
            detectionModule.initialization();
        }
        private void selectCNNdetector(int x, int y)
        {
            usedFeatureExtractor = new CNNFeatureExtractor(x, y, pathToCNNmodelWeights);
            usedClassifier = new CNN_Classifier(x, y);

            // Declatation of main object that perfrom pedestrian detection
            detectionModule = new PedestrianDetectionModule(usedDataset, usedFeatureExtractor, usedClassifier, segmentionFunction, predictionThreadCount);

        }
        private void selectCombindeACFCNNdetector(int x, int y)
        {
            // Declatation of used feature extractor 
            ACF_FeatureExtractor acfFeatureExtractor = new ACF_FeatureExtractor(new System.Drawing.Size(x, y)
                                                                   , new System.Drawing.Size(16, 16)
                                                                   , new System.Drawing.Size(8, 8)
                                                                   , new System.Drawing.Size(8, 8)
                                                                   , 9);
            CNNFeatureExtractor cnnFeatureExtractor = new CNNFeatureExtractor(x, y, pathToCNNmodelWeights);

            usedFeatureExtractor = new ACF_CNN_FeatureExtractor(acfFeatureExtractor, cnnFeatureExtractor);

            // AdaBoost classifier parameters
            MCvBoostParams boostParameters = new MCvBoostParams();
            boostParameters.boostType = Emgu.CV.ML.MlEnum.BOOST_TYPE.REAL;
            boostParameters.weakCount = 2048;
            boostParameters.weightTrimRate = 0.95; // Note : to turn off set to 0
            boostParameters.maxDepth = 2;
            boostParameters.useSurrogates = false;
            boostParameters.maxCategories = 2;
            boostParameters.minSampleCount = 220;


            if (usedClassifier != null) usedClassifier.clearClassifier();
            usedClassifier = new CNN_BOOST_Classifier(boostParameters, x, y);

            // Declatation of main object that perfrom pedestrian detection
            detectionModule = new PedestrianDetectionModule(usedDataset, usedFeatureExtractor, usedClassifier, segmentionFunction, predictionThreadCount);

            // Prepare database for test, train the classifier or read from file  
            detectionModule.initialization();
        }

        public void drawImages(Image<Bgr, Byte>[] images)
        {

            image1.Source = ToBitmapSource(images[0].Resize(320, 240, Emgu.CV.CvEnum.INTER.CV_INTER_AREA));
            image2.Source = ToBitmapSource(images[1].Resize(320, 240, Emgu.CV.CvEnum.INTER.CV_INTER_AREA));
            image3.Source = ToBitmapSource(images[2].Resize(320, 240, Emgu.CV.CvEnum.INTER.CV_INTER_AREA));
            image4.Source = ToBitmapSource(images[3].Resize(320, 240, Emgu.CV.CvEnum.INTER.CV_INTER_AREA));

            image1.Refresh();
            image2.Refresh();
            image3.Refresh();
            image4.Refresh();

        }
        public static BitmapSource ToBitmapSource(IImage image)
        {
            using (System.Drawing.Bitmap source = image.Bitmap)
            {
                IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap

                BitmapSource bs = System.Windows.Interop
                  .Imaging.CreateBitmapSourceFromHBitmap(
                  ptr,
                  IntPtr.Zero,
                  Int32Rect.Empty,
                  System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

                DeleteObject(ptr); //release the HBitmap
                return bs;
            }
        }
    }

    public static class ExtensionMethods
    {
        private static Action EmptyDelegate = delegate() { };

        public static void Refresh(this UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }
    }


}
