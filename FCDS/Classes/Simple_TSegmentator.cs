﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;

namespace FCDS.Classes
{
    class Simple_TSegmentator : TSegmentator
    {
        /* parametry algorytmu */
        public Simple_TSegmentator(FiltrationParameters inputFilParams ) : base (inputFilParams)
        {

        }
        protected override Image<Gray, byte> treshFrame(Image<Gray, byte> sourceImage, int thresh)
        {
            Image<Gray, byte> thresholdedImage = new Image<Gray, byte>(sourceImage.Size);
            CvInvoke.cvThreshold(
                                  sourceImage
                                , thresholdedImage
                                , thresh, 255
                                , Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY
                                );
            return thresholdedImage;
        }

    }
}
