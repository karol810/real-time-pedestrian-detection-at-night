﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace FCDS.Classes
{
    class NTPD_TDataset : TDataset
    {
        public NTPD_TDataset(string path) : base(path)
        {
        }

        public override void createDatasetFolders(int xResolution = 64, int yResolution = 128)
        {
            base.createDatasetFolders(xResolution, yResolution);

            calculatePositiveSubDataset("Train", xResolution, yResolution);
            calculatePositiveSubDataset("Test", xResolution, yResolution);

            calculateNegativeSubDataset("Train", xResolution, yResolution);
            calculateNegativeSubDataset("Test", xResolution, yResolution);

        }

        private void calculatePositiveSubDataset(string typeOfSet, int xRes, int yRes)
        {
            Image<Bgr, Byte> resizedImage, sourceImage;
            int counter;
            String destination;
            string[] Files;
            string srcFile, SourceDir;

            destination = "Pos" + typeOfSet;
            SourceDir = pathToDatabase + destination;

            // pobranie listy plików z katalogu
            Files = Directory.GetFileSystemEntries(SourceDir);

            counter = 1;
            // pętla po wszystkich plikach
            for (int i = 0; i < Files.Length; i++)
            {
                // pobranie pełnej nazwy pliku
                srcFile = Path.GetFullPath(Files[i]);

                // wczytanie obrazu                
                sourceImage = new Image<Bgr, Byte>(srcFile);

                resizedImage = sourceImage.Resize(xRes, yRes, INTER.CV_INTER_CUBIC);
                resizedImage.Save(pathToDatabase + "\\PreparedDataset\\" + typeOfSet + "\\Pos\\" + counter + ".png");
                counter++;
            }
        }

        private void calculateNegativeSubDataset(string typeOfSet, int xRes, int yRes)
        {
            Image<Bgr, Byte> cutedImage, sourceImage;
            int counter;
            String destination;
            string[] Files;
            string srcFile, SourceDir;

            destination = "Neg" + typeOfSet;
            SourceDir = pathToDatabase + destination;

            // pobranie listy plików z katalogu
            Files = Directory.GetFileSystemEntries(SourceDir);

            counter = 1;
            // pętla po wszystkich plikach
            for (int i = 0; i < Files.Length; i++)
            {
                // pobranie pełnej nazwy pliku
                srcFile = Path.GetFullPath(Files[i]);

                // wczytanie obrazu                
                sourceImage = new Image<Bgr, Byte>(srcFile);

                //Przycinanie obrazków i obliczanie HOG
                for (int k = 0; k < 3; k++)
                {
                    for (int l = 0; l < 10; l++)
                    {
                        cutedImage = sourceImage.Copy(new Rectangle(l * 64, k * 128, 64, 128));
                        cutedImage.Save(pathToDatabase + "\\PreparedDataset\\" + typeOfSet + "\\Neg\\" + counter + ".png");
                        counter++;
                    }
                }

            }
        }

        public override List<Rectangle>[] getTestFramesAnnotations()
        {
            List<Rectangle>[] resultingList;
            resultingList = new List<Rectangle>[22];

            return resultingList;
        }

        public override List<Rectangle>[] getTrainFramesAnnotations()
        {
            List<Rectangle>[] resultingList;
            resultingList = new List<Rectangle>[22];

            return resultingList;
        }
    }
}
