﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.ML.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
namespace FCDS.Classes
{
 
    class PedestrianDetectionModule
    {
        private TDataset dataSet;
        private FeatureExtractor fExtractor;
        private Classifier classifier;
        private TSegmentator segmentator;
        private int threadsCount;
        private float acfResizeCoef = 0.1f;
        private float cnnResizeCoef = 0.15f;

        public PedestrianDetectionModule(TDataset iDataSet, FeatureExtractor iFExtractor, Classifier iClassifier, TSegmentator iSegmentator, int iThreadsCount)
        {
            dataSet = iDataSet;
            fExtractor = iFExtractor;
            classifier = iClassifier;
            segmentator = iSegmentator;
            threadsCount = iThreadsCount;
        }

        public void initialization()
        {
            // should be performed once
            dataSet.createDatasetFolders();

            #region Check if classifier exist and load/train
            string pathToClassifier;
            pathToClassifier = dataSet.getWorkDirectory() + classifier.Acronym + "_" + fExtractor.getFullStringOfParameters();

            if (File.Exists(pathToClassifier))
            {
                dataSet.createFeatureDataset(fExtractor);
                classifier.loadFromFile(pathToClassifier);
            }
            else
            {
                dataSet.createFeatureDataset(fExtractor);
                classifier.train(dataSet.getTrainPosFeatures(fExtractor), dataSet.getTrainNegFeatures(fExtractor));
                classifier.saveToFile(dataSet.getWorkDirectory(), fExtractor.getFullStringOfParameters());
            }
            #endregion

            // Egzaminowanie klasyfikatora
            //getDetectionEfficiencyOfClassifier(1000);
        }

        public Image<Bgr, Byte>[] detectionTestsDualThresh(int frameNumber, bool withAnnotations, int threshold, int upperThreshold, double classificationThreshold)
        {
            string[] listOfFiles;
            string fullPathOfFile, directoryPath;
            List<Tuple<Rectangle, int>> ROIs;
            List<Rectangle>[] testFramesAnnotations;
            Image<Bgr, Byte>[] imageList = new Image<Bgr, byte>[4];

            // pobranie listy plików z katalogu
            directoryPath = dataSet.getTestFramesDirectory();
            listOfFiles = Directory.GetFiles(directoryPath);

            // Pobranie pełnej ścieżki wybranego pliku
            fullPathOfFile = Path.GetFullPath(listOfFiles[frameNumber]);

            // Wczytanie obrazu
            Image<Gray, Byte> sourceImage = new Image<Gray, byte>(fullPathOfFile);
            Image<Bgr, Byte> sourceImageBGR = new Image<Bgr, byte>(fullPathOfFile);
            Image<Bgr, Byte> sourceImageBGR2 = new Image<Bgr, byte>(fullPathOfFile);
            Image<Gray, Byte> thresholdedImage = new Image<Gray, byte>(sourceImage.Size);
            Image<Gray, Byte> thresholdedImage2 = new Image<Gray, byte>(sourceImage.Size);

            // Rysowanie pieszych
            if (withAnnotations)
            {
                testFramesAnnotations = dataSet.getTestFramesAnnotations();
                foreach (Rectangle pedestrianAnnotation in testFramesAnnotations[frameNumber])
                {
                    //sourceImageBGR2.Draw(pedestrianAnnotation, new Bgr(255, 0, 0), 1);
                }
            }

            //Segmentacja
            ROIs = segmentator.imageSegmentation(sourceImage, threshold, upperThreshold);
            thresholdedImage = segmentator.getFirstThresholdedImage();
            thresholdedImage2 = segmentator.getSecondThresholdedImage();

            Image<Bgr, Byte> thresholdedMarkedImage = thresholdedImage.Convert<Bgr, Byte>();
            Image<Bgr, Byte> thresholdedMarkedImage2 = thresholdedImage2.Convert<Bgr, Byte>();

            //Klasyfikacja i rysowanie zatwierdzonego ROI na obrazie
            double[] responses;

            Image<Bgr, Byte> acfSample, cnnSample;
            MCvFont usedFont = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_SIMPLEX, 0.3, 0.3);

            int roiCounter;

            roiCounter = 0;

            // Multi-threaded prediction
            if (classifier.Acronym == "CNN")
                responses = predictMultipleForCNN(ROIs, sourceImageBGR);
            else if (classifier.Acronym == "CNN_ECVBOOST")
            {
                responses = new double[ROIs.Count];
                for (int ROIindex = 0; ROIindex < ROIs.Count; ROIindex++)
                {
                    acfSample = sourceImageBGR.Copy(prepareScaledRectangle(ROIs[ROIindex].Item1, acfResizeCoef));
                    cnnSample = sourceImageBGR.Copy(prepareScaledRectangle(ROIs[ROIindex].Item1, cnnResizeCoef));

                    acfSample = acfSample.Resize(fExtractor.getResolution().Width, fExtractor.getResolution().Height, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                    cnnSample = cnnSample.Resize(fExtractor.getResolution().Width, fExtractor.getResolution().Height, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);


                    float[] fVector = ((ACF_CNN_FeatureExtractor)fExtractor).calculateFeatrureVector(acfSample, cnnSample);
                    responses[ROIindex] = classifier.predictRespone(fVector);
                    acfSample.Dispose();
                    cnnSample.Dispose();
                }
            }
            else
                responses = predictMultiThreaded(threadsCount, ROIs, sourceImageBGR);

            for (int i=0; i<ROIs.Count; i++)
            {                 
                if (ROIs[i].Item2 == 1) thresholdedMarkedImage.Draw(ROIs[i].Item1, new Bgr(0, 0, 255), 1);
                else thresholdedMarkedImage2.Draw(ROIs[i].Item1, new Bgr(0, 0, 255), 1);
                if (responses[i] > classificationThreshold)
                {
                    sourceImageBGR2.Draw(ROIs[i].Item1, new Bgr(0, 0, 255), 1);
                    
                }
                roiCounter++;
                    //CvInvoke.cvPutText(sample, responses[i].ToString(), new Point(2, 10), ref usedFont, new MCvScalar(0, 1, 255));
                    //sample = sample.Resize(8, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                    //sample.Save("C:\\Materialy\\Bazy_danych\\CVC-14\\Night\\FIR\\CVC_14_database\\ExampleForTest\\" + roiCounter + ".png");
                    //sample.Dispose();
            }

            // Rysowanie informacji o tym, czy liczba ROI była redukowana
            if (segmentator.getReducedROIs())
            {
                CvInvoke.cvPutText(sourceImageBGR2, "Zmniejszona liczba ROI", new Point(2, 10), ref usedFont, new MCvScalar(0, 1, 255));
            }

            // Tworzenie listy obrazków
            imageList[0] = sourceImage.Convert<Bgr, Byte>();
            imageList[1] = sourceImageBGR2;
            imageList[2] = thresholdedMarkedImage;
            imageList[3] = thresholdedMarkedImage2;

            // Zapis do pliku
            Image<Bgr, Byte> combainedImage = new Image<Bgr, Byte>(3 * imageList[0].Width, imageList[0].Height);

            combainedImage.ROI = new Rectangle(0, 0, imageList[0].Width, imageList[0].Height);
            CvInvoke.cvCopy(imageList[1], combainedImage, IntPtr.Zero);
            combainedImage.ROI = new Rectangle(imageList[0].Width, 0, imageList[0].Width, imageList[0].Height);
            CvInvoke.cvCopy(imageList[2], combainedImage, IntPtr.Zero);
            combainedImage.ROI = new Rectangle(2 * imageList[0].Width, 0, imageList[0].Width, imageList[0].Height);
            CvInvoke.cvCopy(imageList[3], combainedImage, IntPtr.Zero);
            //combainedImage.ROI = new Rectangle(3 * imageList[0].Width, 0, imageList[0].Width, imageList[0].Height);
            //CvInvoke.cvCopy(imageList[1], combainedImage, IntPtr.Zero);
            combainedImage.ROI = new Rectangle(0, 0, 3 * imageList[0].Width, imageList[0].Height);
            combainedImage.Save(dataSet.getTestPosDirectiory() + "DetectionImages//" + frameNumber + ".png");

            combainedImage.Dispose();

            return imageList;
        }

        public float[] fullScaleDetectionTest(int threshold, int higherThreshold, double classificationThreshold, int thresholdingMethod=2, string testInfo="")
        {
            string[] listOfFiles;
            int[] ROIcounterTable;
            string fullPathOfFile, directoryPath;
            int classifiedDetections, correctDetections, unverifiedPedestrians, annotatedPedestriansCount;
            double rejected;
            Stopwatch timer = new Stopwatch();
            double[] responses;

            List<Tuple<Rectangle, int>> ROIs;
            List<Rectangle>[] testFramesAnnotations;
            List<bool> undetectedPedestrians, undetectedPedestrians2;
            List<int> framesWithUndetectedPedestrians;
            List<Tuple<int, double, int>> predictionsForNegSamples;
            List<Tuple<int, double, Rectangle>> predictionsForPosSamples;
            Image<Bgr, Byte> acfSample, cnnSample;
            int width, height, sampleCounter;

            classifiedDetections = 0;
            annotatedPedestriansCount = 0;
            correctDetections = 0;
            rejected = 0;
            unverifiedPedestrians = 0;
            sampleCounter = 0;

            int sumOfUndetectedPedestrians = 0;
            predictionsForPosSamples = new List<Tuple<int, double, Rectangle>>();
            predictionsForNegSamples = new List<Tuple<int, double, int>>();

            framesWithUndetectedPedestrians = new List<int>();
            ROIcounterTable = new int[segmentator.filtrationParams.maxROIcount+1];
            for (int i=0; i< segmentator.filtrationParams.maxROIcount + 1; i++) { ROIcounterTable[i] = 0; }

            // pobranie listy plików z katalogu
            directoryPath = dataSet.getTestFramesDirectory();
            listOfFiles = Directory.GetFiles(directoryPath);

            // Pobieranie opisu zdjęć
            testFramesAnnotations = dataSet.getTestFramesAnnotations();
            foreach(List<Rectangle> PedestriansInImage in testFramesAnnotations)
            {
                foreach(Rectangle pedestrian in PedestriansInImage)
                {
                    if (pedestrian.Width * pedestrian.Height < segmentator.getMinPedestrianArea())
                    {
                        PedestriansInImage.Remove(pedestrian);
                    }
                    else annotatedPedestriansCount++;
                }
            }

            timer.Start();

            #region Proces detekcji
            for (int i = 0; i < listOfFiles.Length -1; i++)
            {
                // Pobranie pełnej ścieżki wybranego pliku
                fullPathOfFile = Path.GetFullPath(listOfFiles[i]);

                // Wczytanie obrazu
                using (Image<Gray, Byte> sourceImage = new Image<Gray, byte>(fullPathOfFile))
                {
                    //Segmentacja
                    using (Image<Bgr, Byte> sourceImageBGR = new Image<Bgr, byte>(fullPathOfFile))
                    {

                        undetectedPedestrians = new List<bool>();
                        undetectedPedestrians2 = new List<bool>();


                        for (int j = 0; j < testFramesAnnotations[i].Count; j++)
                        {
                            width = testFramesAnnotations[i][j].Width;
                            height = testFramesAnnotations[i][j].Height;
                            undetectedPedestrians.Add(true);
                            undetectedPedestrians2.Add(true);
                        }

                        ROIs = segmentator.imageSegmentation(sourceImage, threshold, higherThreshold);
                        sampleCounter = sampleCounter + ROIs.Count;

                        // Multi-threaded prediction
                        if (classifier.Acronym == "CNN")
                            responses = predictMultipleForCNN(ROIs, sourceImageBGR);
                        else if (classifier.Acronym == "CNN_ECVBOOST")
                        {
                            responses = new double[ROIs.Count];
                            for(int ROIindex = 0; ROIindex < ROIs.Count; ROIindex++)
                            {
                                acfSample = sourceImageBGR.Copy(prepareScaledRectangle(ROIs[ROIindex].Item1, acfResizeCoef));
                                cnnSample = sourceImageBGR.Copy(prepareScaledRectangle(ROIs[ROIindex].Item1, cnnResizeCoef));

                                acfSample = acfSample.Resize(fExtractor.getResolution().Width, fExtractor.getResolution().Height, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                                cnnSample = cnnSample.Resize(fExtractor.getResolution().Width, fExtractor.getResolution().Height, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);


                                float[] fVector = ((ACF_CNN_FeatureExtractor)fExtractor).calculateFeatrureVector(acfSample, cnnSample);
                                responses[ROIindex] = classifier.predictRespone(fVector);
                                acfSample.Dispose();
                                cnnSample.Dispose();

                                GC.Collect();
                                GC.WaitForPendingFinalizers();
                                GC.Collect();
                            }
                        }
                        else
                            responses = predictMultiThreaded(threadsCount, ROIs, sourceImageBGR);

                        timer.Stop();

                        //Klasyfikacja i sprawdzanie poprawności
                        for (int j = 0; j < ROIs.Count; j++)
                        {
                            if (responses[j] > classificationThreshold)
                            {
                                classifiedDetections++;

                                //Sprawdzianie poprawności z opisem bazy
                                if (checkConvergenceOfDetection(ROIs[j].Item1, testFramesAnnotations[i], undetectedPedestrians))
                                {
                                    //Rectangle upScalledROI = prepareScaledRectangle(ROIs[j].Item1, cnnResizeCoef);
                                    //Image<Bgr, Byte> sample = sourceImageBGR.Copy(upScalledROI);
                                    //sample = sample.Resize(64, 128, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                                    //sample.Save("C:\\Materialy\\Bazy_danych\\CVC-14\\Night\\FIR\\PreparedDataset\\Test\\Pos_from_detection\\" + posCount + ".png");
                                    //posCount++;
                                    //sample.Dispose();
                                    correctDetections++;
                                }
                                else
                                {
                                    //Rectangle upScalledROI = prepareScaledRectangle(ROIs[j].Item1, cnnResizeCoef);
                                    //Image<Bgr, Byte> sample = sourceImageBGR.Copy(upScalledROI);
                                    //sample = sample.Resize(64, 128, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                                    //sample.Save("C:\\Materialy\\Bazy_danych\\CVC-14\\Night\\FIR\\PreparedDataset\\Test\\Neg_from_detection\\" + negCount + ".png");
                                    //negCount++;
                                    //sample.Dispose();
                                }
                                                      
                            }

                            // Prepare sets for calculation of DET
                            if (checkConvergenceOfDetection(ROIs[j].Item1, testFramesAnnotations[i], undetectedPedestrians2))
                            {
                                predictionsForPosSamples.Add(new Tuple<int, double, Rectangle>(i, responses[j], ROIs[j].Item1));

                                //Rectangle upScalledROI = prepareScaledRectangle(ROIs[j].Item1, acfResizeCoef);
                                //Image<Bgr, Byte> sample = sourceImageBGR.Copy(upScalledROI);
                                //sample = sample.Resize(64, 128, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                                //sample.Save("C:\\Materialy\\Bazy_danych\\CVC-14\\Night\\FIR\\PreparedDataset\\Test\\Pos_from_detection_test_" + iterator + "\\" + imageIterator + ".png");
                                //imageIterator++;

                            }                                
                            else
                                predictionsForNegSamples.Add(new Tuple<int, double, int>(i, responses[j], ROIs[j].Item2));
                        }

                        timer.Start();

                        //drawTestImage(sourceImageBGR, testFramesAnnotations[i], intersectionsWithAnnotations, i);

                        sourceImageBGR.Dispose();
                        GC.SuppressFinalize(sourceImageBGR);
                    }

                    sourceImage.Dispose();
                    GC.SuppressFinalize(sourceImage);

                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                }
                timer.Stop();

                sumOfUndetectedPedestrians = 0;
                foreach (bool undetectedPed in undetectedPedestrians)
                {
                    if (undetectedPed) sumOfUndetectedPedestrians++;
                }
                unverifiedPedestrians = unverifiedPedestrians + sumOfUndetectedPedestrians;

                if (sumOfUndetectedPedestrians > 0) framesWithUndetectedPedestrians.Add(i);

                timer.Start();
            }

            timer.Stop();

            #endregion

            #region Prepare DET results
            double falsePredictCount;
            double faslePredictRatio, detectionRate;
            double difference, differenceMIN;
            int iterations = 500;
            double tempThresh;
            double[,] probsOfDET = new double[iterations + 1, 3];
            differenceMIN = 10;

            for (int i = 1; i < iterations; i++)
            {
                int undetectedPedsCount = 0;
                falsePredictCount = 0;

                tempThresh = (i) / (double)(iterations);

                // calculation for negative samples
                for (int k = 0; k < predictionsForNegSamples.Count; k++)
                {
                    if (predictionsForNegSamples[k].Item2 > tempThresh)
                    {
                        if (predictionsForNegSamples[k].Item3 < 4) falsePredictCount++;
                        // for mother ROIs
                        else if (predictionsForNegSamples[k].Item3 % 2 == 0) falsePredictCount++;
                        // find if divided ROIs are classified as negative also
                        else
                        {
                            // find response of mother ROIs
                            for (int falseIndex = 0; falseIndex < predictionsForNegSamples.Count; falseIndex++)
                            {
                                if (predictionsForNegSamples[falseIndex].Item3 == predictionsForNegSamples[k].Item3 - 1)
                                {
                                    // predictionsForNegSamples[falseIndex] is mother ROI
                                    if (predictionsForNegSamples[k].Item1
                                        == predictionsForNegSamples[falseIndex].Item1)
                                    {
                                        // check mother ROI
                                        if (predictionsForNegSamples[falseIndex].Item2 <= tempThresh
                                            && predictionsForNegSamples[k].Item2 > tempThresh)
                                            falsePredictCount++;
                                    }
                                }
                            }

                        }
                    }
                }

                // estimating the undetected pedestirans
                for (int frameIndex = 0; frameIndex < listOfFiles.Length - 1; frameIndex++)
                {

                    // calculation for positive samples
                    undetectedPedestrians = new List<bool>();
                    for (int j = 0; j < testFramesAnnotations[frameIndex].Count; j++)
                    {
                        width = testFramesAnnotations[frameIndex][j].Width;
                        height = testFramesAnnotations[frameIndex][j].Height;
                        undetectedPedestrians.Add(true);
                    }

                    //Klasyfikacja i sprawdzanie poprawności
                    foreach (Tuple<int, double, Rectangle> positiveROI in predictionsForPosSamples)
                    {
                        if (positiveROI.Item1 == frameIndex && positiveROI.Item2 > tempThresh)
                            checkConvergenceOfDetection(positiveROI.Item3, testFramesAnnotations[frameIndex], undetectedPedestrians);                        
                    }

                    sumOfUndetectedPedestrians = 0;
                    foreach (bool undetectedPed in undetectedPedestrians)
                    {
                        if (undetectedPed) sumOfUndetectedPedestrians++;
                    }
                    undetectedPedsCount = undetectedPedsCount + sumOfUndetectedPedestrians;
                }

                //Obliczanie prawdopodobieństw
                faslePredictRatio = falsePredictCount / (double)(listOfFiles.Length);
                detectionRate = ((double)annotatedPedestriansCount - (double)undetectedPedsCount) / (double)annotatedPedestriansCount;

                //Przypisywanie zmiennych do tablicy
                probsOfDET[i, 0] = detectionRate;
                probsOfDET[i, 1] = faslePredictRatio;
                probsOfDET[i, 2] = tempThresh;
                
                //Find thresh 
                difference = Math.Abs(faslePredictRatio - 1);
                if (difference < differenceMIN)
                {
                    differenceMIN = difference;
                    probsOfDET[0, 0] = detectionRate;
                    probsOfDET[0, 1] = faslePredictRatio;
                    probsOfDET[0, 2] = tempThresh;
                }
            }

            String[] lines = new String[probsOfDET.Length];
            for (int i = 0; i < iterations; i++)
            {
                lines[i] = probsOfDET[i, 0].ToString() + " " + probsOfDET[i, 1].ToString() + " " + probsOfDET[i, 2].ToString();
            }
            #endregion

            #region Opracowywanie wyników i zapis do pliku

            double detectionRatio, FAR, FPPW;

            detectionRatio = ((double)annotatedPedestriansCount - (double)unverifiedPedestrians) / (double)annotatedPedestriansCount;

            FAR = (classifiedDetections - correctDetections) /
                (rejected + (classifiedDetections - correctDetections));

            FPPW = ((double)classifiedDetections - correctDetections) / listOfFiles.Length;

            string framesWithUndetectedPedes = "";
            foreach (int frameNum in framesWithUndetectedPedestrians) framesWithUndetectedPedes = framesWithUndetectedPedes + " " + frameNum;

            // Write result to file
            using (StreamWriter outputFile = new StreamWriter(
                dataSet.getWorkDirectory()
                + "FullScaleDetectionResult_"
                + classifier.Acronym + "_"
                + fExtractor.getResolution().Width
                + "_" + fExtractor.getResolution().Height
                + "_" + threshold + "_" + higherThreshold
                + "_" + testInfo
                + ".txt"))
            {
                outputFile.WriteLine("DR " + detectionRatio);
                outputFile.WriteLine("FAR " + FAR);
                outputFile.WriteLine("FPPW " + FPPW);
                outputFile.WriteLine("Time(ms) " + (timer.ElapsedMilliseconds / listOfFiles.Length));
                outputFile.WriteLine("Samples per frame:" + (sampleCounter / listOfFiles.Length));
                outputFile.WriteLine("Frames with undetected pedestrians:" + framesWithUndetectedPedes);
                outputFile.WriteLine("------------------------------------------------------------------------");
                outputFile.WriteLine("DR            FPPW            Threshold");

                for (int i = 0; i < iterations; i++)
                {
                    outputFile.WriteLine(lines[i]);
                }
            }

            float[] returnStatistics = new float[4];
            returnStatistics[0] = 1 - (float)probsOfDET[0, 0];
            returnStatistics[1] = 1000 * (float) listOfFiles.Length / (float) timer.ElapsedMilliseconds;
            returnStatistics[2] = (timer.ElapsedMilliseconds / listOfFiles.Length);
            returnStatistics[3] = (sampleCounter / listOfFiles.Length);

            return returnStatistics;
            #endregion
        }

        public int[] fullScaleDetectionTestMT(int threshold, int higherThreshold, double classificationThreshold, int thresholdingMethod = 2, string testInfo = "")
        {
            string[] listOfFiles;
            int[] ROIcounterTable;
            string fullPathOfFile, directoryPath;
            int classifiedDetections, correctDetections, unverifiedPedestrians, annotatedPedestriansCount;
            double rejected;
            Stopwatch timer = new Stopwatch();
  
            List<Rectangle>[] testFramesAnnotations;
            List<bool> undetectedPedestrians;
            List<int> framesWithUndetectedPedestrians;

            int width, height, sampleCounter;

            classifiedDetections = 0;
            annotatedPedestriansCount = 0;
            correctDetections = 0;
            rejected = 0;
            unverifiedPedestrians = 0;
            sampleCounter = 0;

            framesWithUndetectedPedestrians = new List<int>();
            ROIcounterTable = new int[50];
            for (int i = 0; i < 50; i++) { ROIcounterTable[i] = 0; }

            // Creating task table
            Task[] threadList = new Task[threadsCount];
            TSegmentator[] segmentatorCopies = new TSegmentator[threadsCount];
            for (int i = 0; i < threadsCount; i++) segmentatorCopies[i] = new Simple_TSegmentator(segmentator.getFiltrationParams());

            // pobranie listy plików z katalogu
            directoryPath = dataSet.getTestFramesDirectory();
            listOfFiles = Directory.GetFiles(directoryPath);

            // Creating ROIs array to store detections for all frames
            List<Tuple<Rectangle, int>>[] detectedROIsArray = new List<Tuple<Rectangle, int>>[listOfFiles.Length];

            // Pobieranie opisu zdjęć
            testFramesAnnotations = dataSet.getTestFramesAnnotations();
            foreach (List<Rectangle> PedestriansInImage in testFramesAnnotations)
            {
                foreach (Rectangle pedestrian in PedestriansInImage)
                {
                    if (pedestrian.Width * pedestrian.Height < segmentator.getMinPedestrianArea())
                    {
                        PedestriansInImage.Remove(pedestrian);
                    }
                    else annotatedPedestriansCount++;
                }
            }

            timer.Start();

            #region Multi-threaded pedestrian detection 
            int threadNumber = 0;
            for (int i = 0; i < listOfFiles.Length - 1; i++)
            {                                
                // Pobranie pełnej ścieżki wybranego pliku
                fullPathOfFile = Path.GetFullPath(listOfFiles[i]);
                Image<Bgr, byte> sourceImage = new Image<Bgr, byte>(fullPathOfFile);

                int index = i;
                
                if (i < threadsCount)
                {
                    int threadIndex = threadNumber;
                    threadList[threadNumber] = Task.Run(() => detectOnFrameThread(segmentatorCopies[threadIndex], ref sourceImage, ref detectedROIsArray[index], threshold, higherThreshold, classificationThreshold));
                    threadNumber++;
                }
                else
                {
                    threadNumber = i % threadsCount;
                    threadList[threadNumber].Wait();

                    int threadIndex = threadNumber;
                    threadList[threadNumber] = Task.Run(() => detectOnFrameThread(segmentatorCopies[threadIndex], ref sourceImage, ref detectedROIsArray[index], threshold, higherThreshold, classificationThreshold));
                }
                
            }

            // Wait for remaining threads
            for (int i = 0; i < threadsCount; i++) threadList[i].Wait();
            
            timer.Stop();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            #endregion

            #region Opracowywanie wyników i zapis do pliku

            double detectionRatio, FAR, FPPW;
            int sumOfUndetectedPedestrians;

            // counting of correct and false detections
            sumOfUndetectedPedestrians = 0;
            classifiedDetections = 0;
            unverifiedPedestrians = 0;
            for (int i = 0; i < listOfFiles.Length - 1; i++)
            {
                classifiedDetections = classifiedDetections + detectedROIsArray[i].Count;

                undetectedPedestrians = new List<bool>();

                for (int j = 0; j < testFramesAnnotations[i].Count; j++)
                {
                    width = testFramesAnnotations[i][j].Width;
                    height = testFramesAnnotations[i][j].Height;
                    undetectedPedestrians.Add(true);
                }

                foreach (Tuple<Rectangle, int> ROI in detectedROIsArray[i])
                {
                    if (checkConvergenceOfDetection(ROI.Item1, testFramesAnnotations[i], undetectedPedestrians)) correctDetections++;
                }

                sumOfUndetectedPedestrians = 0;
                foreach (bool undetectedPed in undetectedPedestrians)
                {
                    if (undetectedPed) sumOfUndetectedPedestrians++;
                }
                unverifiedPedestrians = unverifiedPedestrians + sumOfUndetectedPedestrians;
            }

            detectionRatio = ((double)annotatedPedestriansCount - (double)unverifiedPedestrians) / (double)annotatedPedestriansCount;

            FAR = (classifiedDetections - correctDetections) /
                (rejected + (classifiedDetections - correctDetections));

            FPPW = ((double)classifiedDetections - correctDetections) / listOfFiles.Length;

            string framesWithUndetectedPedes = "";
            foreach (int frameNum in framesWithUndetectedPedestrians) framesWithUndetectedPedes = framesWithUndetectedPedes + " " + frameNum;

            // Write result to file
            using (StreamWriter outputFile = new StreamWriter(dataSet.getWorkDirectory() + "FullScaleDetectionResult_" + classifier.Acronym + "_" + threshold + "_" + higherThreshold + ".txt"))
            {
                outputFile.WriteLine("DR " + detectionRatio);
                outputFile.WriteLine("FAR " + FAR);
                outputFile.WriteLine("FPPW " + FPPW);
                outputFile.WriteLine("Time(ms) " + (timer.ElapsedMilliseconds / listOfFiles.Length));
                outputFile.WriteLine("Samples per frame:" + (sampleCounter / listOfFiles.Length));
                outputFile.WriteLine("Frames with undetected pedestrians:" + framesWithUndetectedPedes);
            }

            return ROIcounterTable;
            #endregion
        }

        private bool checkConvergenceOfDetection(Rectangle detectedPedestrian, List<Rectangle> annotations, List<bool> iUndetectedPedestrians)
        {
            bool finalResponse = false;
            double detectedPedArea = detectedPedestrian.Height * detectedPedestrian.Width;
            double annotatedRectArea, intersectionArea, areaCoeff, interAreaCoeff;

            //Sprawdzanie czy prostokąty sie pokrywają i w jakim stopniu
            for (int i = 0; i < annotations.Count; i++)
            {
                if (detectedPedestrian.IntersectsWith(annotations[i]))
                {
                    annotatedRectArea = annotations[i].Height * annotations[i].Width;
                    areaCoeff = detectedPedArea / annotatedRectArea;

                    Rectangle intersection = Rectangle.Intersect(detectedPedestrian, annotations[i]);
                    intersectionArea = intersection.Height * intersection.Width;
                    interAreaCoeff = intersectionArea / annotatedRectArea;

                    if (interAreaCoeff > 0.4)
                    {
                        // Ostateczna decyzja
                        if (areaCoeff < 4.5 && areaCoeff > 0.4)
                        {
                            finalResponse = true;
                            iUndetectedPedestrians[i] = false;
                        }
                    }
                    else if (intersectionArea / detectedPedArea > 0.8)
                        finalResponse = true;

                }
            }

            return finalResponse;
        }

        private double[] predictForTestSet(float[][] subDataset)
        {
            int subDatasetSize, featureSize;
            subDatasetSize = subDataset.Length;
            featureSize = subDataset[0].Length;
            double[] response = new double[subDatasetSize];

            //Przewidywanie
            for (int i = 0; i < subDatasetSize; i++)
            {
                response[i] = classifier.predictRespone(subDataset[i]);
            }

            return response;
        }

        private double[] predictMultiThreaded(int threadsCount, List<Tuple<Rectangle, int>> ROIsList, Image<Bgr, byte> sourceImage)
        {
            int usedThreads = 0;
            int threadToJoin = 0;
                     
            // check if the ROIs number greater than ROIs count
            if (threadsCount > ROIsList.Count) threadsCount = ROIsList.Count;
            Image<Bgr, byte>[] sourceImages = new Image<Bgr, byte>[ROIsList.Count];
            for (int i = 0; i < ROIsList.Count; i++)
            {
                sourceImages[i] = sourceImage.Copy(prepareScaledRectangle(ROIsList[i].Item1, acfResizeCoef));
                sourceImages[i] = sourceImages[i].Resize(fExtractor.getResolution().Width, fExtractor.getResolution().Height, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
            }
            // Create thread for each ROI
            Task[] threadList = new Task[threadsCount];
            double[] predictionVector = new double[ROIsList.Count];
            for( int i = 0; i < ROIsList.Count; i++)
            {
                if(threadsCount == ROIsList.Count)
                {
                    int index = i;
                    threadList[i% threadsCount] = Task.Run( () => predictThread(sourceImages[index], ref predictionVector[index], index));
                }
                else
                {
                    if (usedThreads < threadsCount)
                    {
                        int index = i;
                        threadList[i% threadsCount] = Task.Run(() => predictThread(sourceImages[index], ref predictionVector[index], index));
                        usedThreads++;
                    }
                    else
                    {
                        threadList[threadToJoin].Wait();

                        usedThreads = threadToJoin;
                        threadToJoin++;
                        if (threadToJoin >= threadsCount) threadToJoin = threadToJoin - threadsCount;

                        int index = i;
                        threadList[usedThreads] = new Task(() => predictThread(sourceImages[index], ref predictionVector[index], index));
                        threadList[usedThreads].Start();
                    }
                }
            }

            // End remaining threads 
            if (threadsCount == ROIsList.Count)
            {
                for (int i = 0; i < ROIsList.Count; i++)
                {
                    threadList[i].Wait();
                }
            }
            else
            {
                for (int i = threadToJoin; i < threadsCount; i++)
                {
                    threadList[i].Wait();
                }
            }
            for (int i = 0; i < ROIsList.Count; i++) sourceImages[i].Dispose();
            return predictionVector;
        }

        private double[] predictMultipleForCNN(List<Tuple<Rectangle, int>> ROIsList, Image<Bgr, byte> sourceImage)
        {
            double[] responseVector;
            if (ROIsList.Count > 0)
            {
                Image<Bgr, byte>[] samples = new Image<Bgr, byte>[ROIsList.Count];
                for (int i = 0; i < ROIsList.Count; i++)
                {
                    Rectangle upSizedROI = prepareScaledRectangle(ROIsList[i].Item1, cnnResizeCoef);
                    samples[i] = sourceImage.Copy(upSizedROI).Resize(fExtractor.getResolution().Width, fExtractor.getResolution().Height, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                }
                responseVector = ((CNNFeatureExtractor)fExtractor).calculateMultipleFeatrureVectors(samples);

                for (int i = 0; i < ROIsList.Count; i++) samples[i].Dispose();
            }
            else responseVector = null;
            
            return responseVector;
        }
        private void predictThread(Image<Bgr, byte> sourceImage, ref double predictionValue, int index )
        {
                float[] RoiFVector = fExtractor.calculateFeatrureVector(sourceImage);
                predictionValue = classifier.predictRespone(RoiFVector);            
        }

        private void detectOnFrameThread(TSegmentator segmentatorClone, ref Image<Bgr, byte> sourceImageBGR, ref List<Tuple<Rectangle, int>> listOfROIs, int threshold, int higherThreshold, double classificationThreshold)
        {
            double response;

            float[] RoiFVector;

            Image<Gray, Byte> sourceImage = sourceImageBGR.Convert<Gray, Byte>();
            Image<Bgr, Byte> sample;

            // ROIs = segmentator.twoThresholdSegmentation(sourceImage, threshold, higherThreshold, true, true, 30, true);
            List<Tuple<Rectangle, int>> segmentedROIs = segmentatorClone.imageSegmentation(sourceImage, threshold, higherThreshold);
            sourceImage.Dispose();

            listOfROIs = new List<Tuple<Rectangle, int>>();

            //Klasyfikacja i sprawdzanie poprawności
            foreach (Tuple<Rectangle, int> ROI in segmentedROIs)
            {
                sample = sourceImageBGR.Copy(ROI.Item1);
                sample = sample.Resize(fExtractor.getResolution().Width, fExtractor.getResolution().Height, Emgu.CV.CvEnum.INTER.CV_INTER_AREA);
                RoiFVector = fExtractor.calculateFeatrureVector(sample);
                response = classifier.predictRespone(RoiFVector);

                sample.Dispose();

                if (true) //response > classificationThreshold)
                {
                    listOfROIs.Add(ROI);
                }
            }

            sourceImageBGR.Dispose();

            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();

        }
        private Rectangle prepareScaledRectangle(Rectangle ROI, float resizeCoef)
        {
            int newX, newY, newWidth, newHeight;
            Rectangle newROI;

            newX = (int)(ROI.X - resizeCoef * ROI.Width);
            newY = (int)(ROI.Y - resizeCoef * ROI.Height);
            newWidth = (int)(ROI.Width + 2 * resizeCoef * ROI.Width);
            newHeight = (int)(ROI.Height + 2 * resizeCoef * ROI.Height);

            newROI = new Rectangle(newX, newY, newWidth, newHeight);

            if (newX < 0) newROI = ROI;
            if (newY < 0) newROI = ROI;
            if (newX + newWidth > 639) newROI = ROI;
            if (newY + newHeight > 470) newROI = ROI;

            return newROI;
        }

        public void setOptimalParametersDoubleThresholding()
        {
            segmentator.filtrationParams.homogenousRegionsCoef = 24;
            segmentator.filtrationParams.minArea = 150;
            segmentator.filtrationParams.minHeightCoef = 0.35f;
            segmentator.filtrationParams.minRatio = 1f;
            segmentator.filtrationParams.maxRatio = 6.5f;
            segmentator.filtrationParams.similatiry = 0.65f;
            segmentator.filtrationParams.skewnees = 0.15f;
            segmentator.filtrationParams.fulfilment = 0.35f;
            segmentator.filtrationParams.minAreaInit = 15;
            segmentator.filtrationParams.maxROIcount = 40;
        }
        public void setOptimalBestParametersDoubleThresholding()
        {
            segmentator.filtrationParams.homogenousRegionsCoef = 28;
            segmentator.filtrationParams.minArea = 250;
            segmentator.filtrationParams.minHeightCoef = 0.45f;
            segmentator.filtrationParams.minRatio = 1f;
            segmentator.filtrationParams.maxRatio = 6.5f;
            segmentator.filtrationParams.similatiry = 0.65f;
            segmentator.filtrationParams.skewnees = 0.14f;
            segmentator.filtrationParams.fulfilment = 0.2f;
            segmentator.filtrationParams.minAreaInit = 30;
            segmentator.filtrationParams.maxROIcount = 40;
        }
        public void setBestParametersDoubleThresholding()
        {
            segmentator.filtrationParams.homogenousRegionsCoef = 20;
            segmentator.filtrationParams.minArea = 100;
            segmentator.filtrationParams.minHeightCoef = 0.35f;
            segmentator.filtrationParams.minRatio = 0.8f;
            segmentator.filtrationParams.maxRatio = 7f;
            segmentator.filtrationParams.similatiry = 0.8f;
            segmentator.filtrationParams.skewnees = 0.17f;
            segmentator.filtrationParams.fulfilment = 0.2f;
            segmentator.filtrationParams.minAreaInit = 12;
            segmentator.filtrationParams.maxROIcount = 100;
        }
        public void setOptimalParametersTripleThresholding()
        {
            segmentator.filtrationParams.homogenousRegionsCoef = 22;
            segmentator.filtrationParams.minArea = 150;
            segmentator.filtrationParams.minHeightCoef = 0.35f;
            segmentator.filtrationParams.minRatio = 1.2f;
            segmentator.filtrationParams.maxRatio = 5.5f;
            segmentator.filtrationParams.similatiry = 0.7f;
            segmentator.filtrationParams.skewnees = 0.11f;
            segmentator.filtrationParams.fulfilment = 0.35f;
            segmentator.filtrationParams.minAreaInit = 15;
            segmentator.filtrationParams.maxROIcount = 70;
        }
        public void setBestParametersTripleThresholding()
        {
            segmentator.filtrationParams.homogenousRegionsCoef = 22;
            segmentator.filtrationParams.minArea = 100;
            segmentator.filtrationParams.minHeightCoef = 0.35f;
            segmentator.filtrationParams.minRatio = 1f;
            segmentator.filtrationParams.maxRatio = 5.5f;
            segmentator.filtrationParams.similatiry = 0.8f;
            segmentator.filtrationParams.skewnees = 0.14f;
            segmentator.filtrationParams.fulfilment = 0.3f;
            segmentator.filtrationParams.minAreaInit = 9;
            segmentator.filtrationParams.maxROIcount = 100;
        }

        public void updateSegmentator(TSegmentator newSegmentator)
        {
            segmentator = newSegmentator;
        }
    }
}
