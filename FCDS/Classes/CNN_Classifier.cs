﻿ using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCDS.Classes
{
    class CNN_Classifier : Classifier
    {
        // this class is an artificial overlay - true CNN as a feature extractor

        public CNN_Classifier(int sampleWidth, int sampleHeight)
        {
            acronym = "CNN";
        }

        public override double predictRespone(float[] sampleToTest)
        {
            double response = sampleToTest[0];
            return response;
        }

        public override void saveToFile(string pathToWorkDirrectory, string acronymOfExtractor)
        {
            // CNN only for read from file
        }

        public override void train(float[][] trainPosFeatures, float[][] trainNegFeatures)
        {

        }

        public override void loadFromFile(string pathToClassifier)
        {

        }

        public override Classifier deepCopy()
        {
            Classifier temporary = new LIBSVM_Classifier();
            return temporary;
        }

        public override void clearClassifier()
        {

        }
    }
}
