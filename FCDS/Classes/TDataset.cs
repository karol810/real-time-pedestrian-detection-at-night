﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace FCDS.Classes
{
    abstract class TDataset
    {
        protected string pathToDatabase;
        protected int numberOfTestPedestrians;
        protected bool isDatasetFoldersPrepared;

        public TDataset(string path)
        {
            pathToDatabase = path;
        }        
        public void createFeatureDataset(FeatureExtractor fExtractor)
        {
            #region Check if database exist

            bool trainPos = false, trainNeg = false, testPos = false, testNeg = false;            
            string acronym = fExtractor.getFullStringOfParameters();
            string pathToFile = pathToDatabase + "PreparedDataset\\Work\\trainPos_" + acronym;
            if (File.Exists(pathToFile))
            {
                trainPos = true;
            }

            pathToFile = pathToDatabase + "PreparedDataset\\Work\\trainNeg_" + acronym;
            if (File.Exists(pathToFile))
            {
                trainNeg = true;
            }

            pathToFile = pathToDatabase + "PreparedDataset\\Work\\testPos_" + acronym;
            if (File.Exists(pathToFile))
            {
                testPos = true;
            }

            pathToFile = pathToDatabase + "PreparedDataset\\Work\\testNeg_" + acronym;
            if (File.Exists(pathToFile))
            {
                testNeg = true;
            }

            #endregion

            String subDestination;
            string sourceDir;

            #region Create positive train dataset        
            float[][] features;
            if (trainPos == false)
            {
                subDestination = "PreparedDataset\\Train\\Pos";
                sourceDir = pathToDatabase + subDestination;

                features = calculateFeaturesSubset(sourceDir, fExtractor);
                pathToFile = pathToDatabase + "PreparedDataset\\Work\\trainPos_" + acronym;
                serializeData(pathToFile, features);
            }
            #endregion

            #region Create negative train dataset

            if (trainNeg == false)
            {
                subDestination = "PreparedDataset\\Train\\Neg";
                sourceDir = pathToDatabase + subDestination;

                features = calculateFeaturesSubset(sourceDir, fExtractor);
                pathToFile = pathToDatabase + "PreparedDataset\\Work\\trainNeg_" + acronym;
                serializeData(pathToFile, features);
            }
            #endregion

            #region Create positive test dataset

            if (testPos == false)
            {
                subDestination = "PreparedDataset\\Test\\Pos";
                sourceDir = pathToDatabase + subDestination;

                features = calculateFeaturesSubset(sourceDir, fExtractor);
                pathToFile = pathToDatabase + "PreparedDataset\\Work\\testPos_" + acronym;
                serializeData(pathToFile, features);
            }
            #endregion

            #region Create negative test dataset

            if (testNeg == false)
            {
                subDestination = "PreparedDataset\\Test\\Neg";
                sourceDir = pathToDatabase + subDestination;

                features = calculateFeaturesSubset(sourceDir, fExtractor);
                pathToFile = pathToDatabase + "PreparedDataset\\Work\\testNeg_" + acronym;
                serializeData(pathToFile, features);
            }
            #endregion
        }
        
        protected float[][] calculateFeaturesSubset (String destinstionFolder, FeatureExtractor fExtractor)
        {
            string[] listOfFiles;
            string fullPathOfFile;

            // pobranie listy plików z katalogu
            listOfFiles = Directory.GetFileSystemEntries(destinstionFolder);
            List<float[]> subFeatureDataset = new List<float[]>();

            // pętla po wszystkich plikach
            for (int i = 0; i < listOfFiles.Length; i++)
            {
                if(Path.GetExtension(listOfFiles[i]) == ".png" || Path.GetExtension(listOfFiles[i]) == ".bmp")
                {
                    fullPathOfFile = Path.GetFullPath(listOfFiles[i]);
                    Image<Bgr, Byte> sourceImage = new Image<Bgr, byte>(fullPathOfFile);
                    Image<Bgr, Byte> resizedImage = sourceImage.Resize(
                                                                    fExtractor.getResolution().Width
                                                                  , fExtractor.getResolution().Height
                                                                  , INTER.CV_INTER_AREA);

                    subFeatureDataset.Add(fExtractor.calculateFeatrureVector(resizedImage));
                    sourceImage.Dispose();
                    resizedImage.Dispose();
                }
            }
            return subFeatureDataset.ToArray();
        }

        static public void serializeData(String path, float[][] myArray)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                formatter.Serialize(stream, myArray);
            }
        }
        static public float[][] deserializeData(String path)
        {
            float[][] myArray;
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                myArray = (float[][])formatter.Deserialize(stream);
            }
            return myArray;
        }

        public virtual void createDatasetFolders(int xResolution = 64, int yResolution = 128)
        {
            #region Check if dataset folders were created
            isDatasetFoldersPrepared = false;
            if (Directory.Exists(pathToDatabase + "\\PreparedDataset"))
            {
                isDatasetFoldersPrepared = true;
                return;
            }
            #endregion

            #region Creating appropriate directiories
            string[] pathsToDatasets = new string[5];
            pathsToDatasets[0] = pathToDatabase + "\\PreparedDataset\\Train\\Pos\\";
            pathsToDatasets[1] = pathToDatabase + "\\PreparedDataset\\Train\\Neg\\";
            pathsToDatasets[2] = pathToDatabase + "\\PreparedDataset\\Test\\Pos\\";
            pathsToDatasets[3] = pathToDatabase + "\\PreparedDataset\\Test\\Neg\\";
            pathsToDatasets[4] = pathToDatabase + "\\PreparedDataset\\Work\\";

            foreach (string path in pathsToDatasets)
            Directory.CreateDirectory(path) ;
            #endregion

        }

        public virtual string calculatePositiveOriginalTestDataset()
        {
            string result = " ";
            return result;
        }

        public abstract List<Rectangle>[] getTestFramesAnnotations();
        public abstract List<Rectangle>[] getTrainFramesAnnotations();

        public string getWorkDirectory()
        {
            string pathToWorkDirectory = pathToDatabase + "PreparedDataset\\Work\\";
            return pathToWorkDirectory;
        }

        public string getTestFramesDirectory()
        {
            string pathToWorkDirectory = pathToDatabase + "PreparedDataset\\Test\\FramesPos\\";
            return pathToWorkDirectory;
        }
        public int getPedestrianNumber()
        {
            return numberOfTestPedestrians;
        }

        public string getTestPosDirectiory()
        {
            string pathToTestPosDirectory = pathToDatabase + "PreparedDataset\\Test\\Pos\\";
            return pathToTestPosDirectory;
        }
        public string getTrainPosDirectiory()
        {
            string pathToTestPosDirectory = pathToDatabase + "PreparedDataset\\Train\\Pos\\";
            return pathToTestPosDirectory;
        }
        public string getTrainNegDirectiory()
        {
            string pathToTestNegDirectory = pathToDatabase + "PreparedDataset\\Train\\Neg\\";
            return pathToTestNegDirectory;
        }

        public Image<Bgr, Byte>[] getImageSamples(int numberOfSamples, int xResolution, int yResolution)
        {
            string[] listOfFiles;
            string fullPathOfFile;
            float[][] subFeatureDataset;
            Image<Bgr, Byte>[] resizedImages;
            Image<Bgr, Byte> rawImage;
            resizedImages = new Image<Bgr, byte>[numberOfSamples];

            string subDestination = "PreparedDataset\\Test\\Pos";
            string sourceDir = pathToDatabase + subDestination;

            // pobranie listy plików z katalogu
            listOfFiles = Directory.GetFileSystemEntries(sourceDir);
            subFeatureDataset = new float[listOfFiles.Length][];

            // pętla po wszystkich plikach
            for (int i = 0; i < numberOfSamples; i++)
            {
                fullPathOfFile = Path.GetFullPath(listOfFiles[i]);
                rawImage = new Image<Bgr, byte>(fullPathOfFile);

                if (rawImage.Width == xResolution
                    && rawImage.Height == yResolution)
                    resizedImages[i] = rawImage;
                else
                    resizedImages[i] = rawImage.Resize(
                                                        xResolution
                                                      , yResolution
                                                      , INTER.CV_INTER_NN);
            }

            return resizedImages;
        }

        public float[][] getTrainPosFeatures(FeatureExtractor fExtractor)
        {
            string acronym = fExtractor.getFullStringOfParameters();
            string pathToFile = pathToDatabase + "PreparedDataset\\Work\\trainPos_" + acronym;

            float[][] returnFeatures;
            returnFeatures = deserializeData(pathToFile);
            return returnFeatures;
        }
        public float[][] getTrainNegFeatures(FeatureExtractor fExtractor)
        {
            string acronym = fExtractor.getFullStringOfParameters();
            string pathToFile = pathToDatabase + "PreparedDataset\\Work\\trainNeg_" + acronym;

            float[][] returnFeatures;
            returnFeatures = deserializeData(pathToFile);
            return returnFeatures;
        }
        public float[][] getTestPosFeatures(FeatureExtractor fExtractor)
        {
            string acronym = fExtractor.getFullStringOfParameters();
            string pathToFile = pathToDatabase + "PreparedDataset\\Work\\testPos_" + acronym;

            float[][] returnFeatures;
            returnFeatures = deserializeData(pathToFile);
            return returnFeatures;
        }
        public float[][] getTestNegFeatures(FeatureExtractor fExtractor)
        {
            string acronym = fExtractor.getFullStringOfParameters();
            string pathToFile = pathToDatabase + "PreparedDataset\\Work\\testNeg_" + acronym;

            float[][] returnFeatures;
            returnFeatures = deserializeData(pathToFile);
            return returnFeatures;
        }

        public string getPathToNativeTrainFramePos()
        {
            string pathToWorkDirectory = pathToDatabase + "Train\\FramesPos\\";
            return pathToWorkDirectory;
        }
    }
}
