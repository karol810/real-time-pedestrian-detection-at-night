﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.ML.Structure;
using System;

namespace FCDS.Classes
{
    class SVM_Classifier : Classifier
    {
        SVM classifier;
        SVMParams parametersOfSVM;

        public SVM_Classifier(SVMParams parameters)
        {
            parametersOfSVM = parameters;
            acronym = "ECVSVM";
        }

        public override double predictRespone(float[] sampleToTest)
        {
            Matrix<float> matrixSample = new Matrix<float>(sampleToTest);
            float response = classifier.Predict(matrixSample);
            return response;
        }

        public override void saveToFile(string pathToWorkDirrectory, string acronymOfExtractor)
        {
            classifier.Save(pathToWorkDirrectory + Acronym + "_" + acronymOfExtractor);
        }

        public override void train(float[][] trainPosFeatures, float[][] trainNegFeatures)
        {
            classifier = new SVM();
            Matrix<float> trainData, trainClasses;
            trainData = prepareTrainData(trainPosFeatures, trainNegFeatures);
            trainClasses = prepareTrainClasses(trainPosFeatures, trainNegFeatures);

            bool trained = classifier.Train(trainData, trainClasses, null, null, parametersOfSVM);
        }
        public void trainAuto(float[][] trainPosFeatures, float[][] trainNegFeatures, int kfold)
        {
            classifier = new SVM();
            Matrix<float> trainData, trainClasses;
            trainData = prepareTrainData(trainPosFeatures, trainNegFeatures);
            trainClasses = prepareTrainClasses(trainPosFeatures, trainNegFeatures);

            bool trained = classifier.TrainAuto(trainData, trainClasses, null, null, parametersOfSVM.MCvSVMParams, kfold);
        }
        public override void loadFromFile(string pathToClassifier)
        {
            classifier = new SVM();
            classifier.Load(pathToClassifier);
        }

        public override Classifier deepCopy()
        {
            Classifier temporary = new SVM_Classifier(this.parametersOfSVM);
            return temporary;
        }

        public override void clearClassifier()
        {

        }
    }
}
