﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Cvb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FCDS.Classes
{
    public struct FiltrationParameters
    {
        public bool morfological;
        public bool filtration;
        public bool regionEnlargement;
        public bool initialBlobSelection;

        public int minArea;
        public int minAreaInit;
        public int homogenousRegionsCoef;
        public int maxROIcount;
        public int thresholdingMethod;

        public float minHeightCoef;
        public float minRatio;
        public float maxRatio;
        public float similatiry;
        public float skewnees;
        public float fulfilment;

    }

    abstract class TSegmentator
    {
        bool areROIsReduced;
        protected StructuringElementEx element;
        protected CvBlobDetector b_det;
        protected CvBlobs bbox;
        protected int bboxSize, minROIarea, thresholdingCount;
        protected int threshold, upperThreshold, thresholdingMethod;
        Image<Gray, Byte> firstThresholdedImage;
        Image<Gray, Byte> secondThresholdedImage;
        Image<Gray, Byte> thirdThresholdedImage;

        List<Tuple<Rectangle, int>> ROIs, combinedROIs;
        Tuple<Rectangle, int>[] ROIsBeforeFiltration;
        bool[] shouldBeRemoved;
        public FiltrationParameters filtrationParams;

        public TSegmentator(FiltrationParameters iFiltrationParams)
        {
            filtrationParams = iFiltrationParams;
            thresholdingMethod = filtrationParams.thresholdingMethod;
            element = new StructuringElementEx(3, 3, 1, 1, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_CROSS);
            b_det = new CvBlobDetector();
            bbox = new CvBlobs();
        }
        
        protected abstract Image<Gray, Byte> treshFrame(Image<Gray, Byte> sourceImage, int threshold);
        
        public List<Tuple<Rectangle, int>> imageSegmentation(Image<Gray, Byte> sourceImage, int iThreshold, int iUpperThreshold)
        {
            threshold = iThreshold;
            upperThreshold = iUpperThreshold;

            if (firstThresholdedImage != null) firstThresholdedImage.Dispose();
            if (secondThresholdedImage != null) secondThresholdedImage.Dispose();
            if (thirdThresholdedImage != null) thirdThresholdedImage.Dispose();

            List<Tuple<Rectangle, int>> ROIs = new List<Tuple<Rectangle, int>>();
            if (thresholdingMethod == 0)
                ROIs = SlidingWindowSegmentation();
            else if (thresholdingMethod == 1)
                ROIs = oneThresholdSegmentation(sourceImage);
            else if (thresholdingMethod == 2)
                ROIs = twoThresholdSegmentation(sourceImage);
            else if (thresholdingMethod == 3)
                ROIs = threeThresholdSegmentation(sourceImage);

            return ROIs;
        }

        private List<Tuple<Rectangle, int>> oneThresholdSegmentation(Image<Gray, Byte> sourceImage)
        {
            thresholdingCount = 0;
            // Create blobs coordination list
            ROIs = new List<Tuple<Rectangle, int>>();

            // Thresholding image
            firstThresholdedImage = treshFrame(sourceImage, threshold);
            processThresholdedImage(firstThresholdedImage);

            processAndFiltrateROIs(sourceImage);

            return ROIs;
        }

        private List<Tuple<Rectangle, int>> twoThresholdSegmentation(Image<Gray, Byte> sourceImage)
        {
            thresholdingCount = 0;
            // Create blobs coordination list
            ROIs = new List<Tuple<Rectangle, int>>();

            // Thresholding image
            firstThresholdedImage = treshFrame(sourceImage, threshold);
            processThresholdedImage(firstThresholdedImage);

            secondThresholdedImage = treshFrame(sourceImage, upperThreshold);
            processThresholdedImage(secondThresholdedImage);

            processAndFiltrateROIs(sourceImage);

            return ROIs;
        }

        private List<Tuple<Rectangle, int>> threeThresholdSegmentation(Image<Gray, Byte> sourceImage)
        {
            thresholdingCount = 0;
            // Create blobs coordination list
            ROIs = new List<Tuple<Rectangle, int>>();

            // Thresholding image
            firstThresholdedImage = treshFrame(sourceImage, threshold);
            processThresholdedImage(firstThresholdedImage);

            secondThresholdedImage = treshFrame(sourceImage, (int)(threshold + upperThreshold) / 2);
            processThresholdedImage(secondThresholdedImage);

            thirdThresholdedImage = treshFrame(sourceImage, upperThreshold);
            processThresholdedImage(thirdThresholdedImage);

            processAndFiltrateROIs(sourceImage);

            return ROIs;
        }

        private List<Tuple<Rectangle, int>> SlidingWindowSegmentation()
        {
            int x, y;
            int x_shift, y_shift;

            int imageWidth = 640;
            int imageHeight = 471;

            int width = 32;
            int height = 64;
            int maxHeight = 420;

            float scale = 1.3f;
            float x_strade = 0.3f;
            float y_strade = 0.15f;

            // Create blobs coordination list
            ROIs = new List<Tuple<Rectangle, int>>();

            while (height < maxHeight)
            {
                x_shift = (int)(width * x_strade);
                y_shift = (int)(height * y_strade);

                // przejście po obrazie
                x = 0;
                y = 0;
                while (y + height < imageHeight)
                {
                    x = 0;
                    while (x + width < imageWidth)
                    {
                        ROIs.Add(new Tuple<Rectangle, int>(new Rectangle(x, y, width, height), 1));
                        x = x + x_shift;
                    }
                    y = y + y_shift;
                }
                // aktualizacja rozmiwaru okna
                width = (int)(scale * width);
                height = (int)(scale * height);
            }
            return ROIs;
        }

        private void processThresholdedImage(Image<Gray, Byte> thresholdedImage)
        {
            thresholdingCount++;

            // Opening image
            if (filtrationParams.morfological) CvInvoke.cvMorphologyEx(thresholdedImage, thresholdedImage, IntPtr.Zero, element, CV_MORPH_OP.CV_MOP_OPEN, 1);

            // Blob detection
            uint res = b_det.Detect(thresholdedImage, bbox);
            bboxSize = bbox.Count + 1;

            if (filtrationParams.initialBlobSelection) initialBlobSelection();

            for (uint i = 0; i < bbox.Count; i++)
            {
                if (shouldBeRemoved == null) ROIs.Add(new Tuple<Rectangle, int>(bbox[i + 1].BoundingBox, thresholdingCount));
                else if (!shouldBeRemoved[i])
                    ROIs.Add(new Tuple<Rectangle, int>(bbox[i + 1].BoundingBox, thresholdingCount));
            }
        }

        private void processAndFiltrateROIs(Image<Gray, Byte> sourceImage)
        {
            ROIsBeforeFiltration = new Tuple<Rectangle, int>[ROIs.Count];
            ROIs.CopyTo(ROIsBeforeFiltration);

            if (filtrationParams.filtration) checkConvergenceOfDetection();

            // Region enlargement
            if (filtrationParams.regionEnlargement) calcRegionEnlargement(2);

            if (filtrationParams.filtration) checkConvergenceOfDetectionForEachElement();

            // Filtration Area
            if (filtrationParams.filtration) blobsFiltration(sourceImage);

            divideWideROIs();
            if (filtrationParams.filtration) checkConvergenceOfDetectionForEachElement();

            areROIsReduced = false;
            if (ROIs.Count > filtrationParams.maxROIcount) apativelyReduceROInumber(sourceImage);
        }

        private void blobsFiltration(Image<Gray, Byte> sourceImage)
        {
            float ratio, minHeight;
            int area;
            int count = ROIs.Count - 1;

            foreach (Tuple<Rectangle, int> ROI in ROIs.ToArray())
            {
                ratio = (float)ROI.Item1.Height / (float)ROI.Item1.Width;
                area = ROI.Item1.Height * ROI.Item1.Width;
                minHeight = filtrationParams.minHeightCoef * (ROI.Item1.Y + ROI.Item1.Height) - 30;

                //// Warunek na wysokość pieszego
                if (ROI.Item1.Height > 330) ROIs.Remove(ROI);
                else if (ROI.Item1.Y > 350) ROIs.Remove(ROI);
                else if (ROI.Item1.Height < minHeight) ROIs.Remove(ROI);

                // Warunek na powierzchnie
                else if (area > 50000 || area < filtrationParams.minArea) ROIs.Remove(ROI);

                //// Warunek na wysokość do szerokości
                else if (ratio > filtrationParams.maxRatio) ROIs.Remove(ROI);
                else if (ratio < filtrationParams.minRatio) ROIs.Remove(ROI);
                //else if (ratio < 1.35 && area > 1200)
                //{
                //    additionalROIs.Add(new Rectangle(ROI.X, ROI.Y, ROI.Width / 3, ROI.Height));
                //    additionalROIs.Add(new Rectangle(ROI.X + ROI.Width / 3, ROI.Y, ROI.Width / 3, ROI.Height));
                //    additionalROIs.Add(new Rectangle(ROI.X + 2 * ROI.Width / 3, ROI.Y, ROI.Width / 3, ROI.Height));
                //    ROIs.Remove(ROI);
                //}
                //else if (ratio < 2 && area > 1200)
                //{
                //    additionalROIs.Add(new Rectangle(ROI.X, ROI.Y, ROI.Width / 2, ROI.Height));
                //    additionalROIs.Add(new Rectangle(ROI.X + ROI.Width / 2, ROI.Y, ROI.Width / 2, ROI.Height));
                //    ROIs.Remove(ROI);
                //}
                //if (ratio < 2) ROIs.RemoveAt(i);
                //if (ratio < 1.2) ROIs.RemoveAt(i);
            }

            foreach (Tuple<Rectangle, int> ROI in ROIs.ToArray())
            {
                filterHomogenousRegions(sourceImage);
            }

        }

        //private void calcRegionEnlargement(int iteration) //Stara wersja bazująca na bbox
        //{
        //    int X, Y, Xr, Yb;
        //    int[] id1, id2;
        //    float ratio;

        //    for (uint i = 1; i < bboxSize; i++)
        //    {
        //        if (bbox.ContainsKey(i))
        //        { 

        //            for (uint j = i + 1; j < bboxSize; j++)
        //            {
        //                if (bbox.ContainsKey(j))
        //                {
        //                    id1 = expandScope(bbox[i].BoundingBox.X, bbox[i].BoundingBox.Right);
        //                    id2 = expandScope(bbox[j].BoundingBox.X, bbox[j].BoundingBox.Right);
        //                    IEnumerable<int> both = id1.Intersect(id2);

        //                    if (both != null && both.GetEnumerator().MoveNext())
        //                    {
        //                        if (bbox[i].BoundingBox.X < bbox[j].BoundingBox.X) X = bbox[i].BoundingBox.X;
        //                        else X = bbox[j].BoundingBox.X;

        //                        if (bbox[i].BoundingBox.Y < bbox[j].BoundingBox.Y) Y = bbox[i].BoundingBox.Y;
        //                        else Y = bbox[j].BoundingBox.Y;

        //                        if (bbox[i].BoundingBox.Bottom > bbox[j].BoundingBox.Bottom) Yb = bbox[i].BoundingBox.Bottom;
        //                        else Yb = bbox[j].BoundingBox.Bottom;

        //                        if (bbox[i].BoundingBox.Right > bbox[j].BoundingBox.Right) Xr = bbox[i].BoundingBox.Right;
        //                        else Xr = bbox[j].BoundingBox.Right;

        //                        ratio = (float)(Yb - Y) / (float)(Xr - X);
        //                        if (ratio > 1.3 && ratio < 6)
        //                        {
        //                            ROIs.Add(new Tuple<Rectangle, int>(new Rectangle( X, Y, Xr - X, Yb - Y), iteration));
        //                        }
        //                    }
        //               }
        //            }
        //        }

        //    }
        //}

        private void calcRegionEnlargement(int iteration)
        {
            int X, Y, Xr, Yb;
            int[] id1, id2;
            float ratio;
            int lowestArea, area1, area2;
            List<Tuple<Rectangle, int>> additionalROIs = new List<Tuple<Rectangle, int>>();

            for (int i = 0; i < ROIs.Count; i++)
            {

                for (int j = i + 1; j < ROIs.Count; j++)
                {
                    id1 = expandScope(ROIs[i].Item1.X, ROIs[i].Item1.Right);
                    id2 = expandScope(ROIs[j].Item1.X, ROIs[j].Item1.Right);
                    IEnumerable<int> both = id1.Intersect(id2);

                    if (both != null && both.GetEnumerator().MoveNext())
                    {
                        area1 = ROIs[i].Item1.Width * ROIs[i].Item1.Height;
                        area2 = ROIs[j].Item1.Width * ROIs[j].Item1.Height;
                        if (area1 > area2) lowestArea = area2;
                        else lowestArea = area1;

                        if (ROIs[i].Item1.X < ROIs[j].Item1.X) X = ROIs[i].Item1.X;
                        else X = ROIs[j].Item1.X;

                        if (ROIs[i].Item1.Y < ROIs[j].Item1.Y) Y = ROIs[i].Item1.Y;
                        else Y = ROIs[j].Item1.Y;

                        if (ROIs[i].Item1.Bottom > ROIs[j].Item1.Bottom) Yb = ROIs[i].Item1.Bottom;
                        else Yb = ROIs[j].Item1.Bottom;

                        if (ROIs[i].Item1.Right > ROIs[j].Item1.Right) Xr = ROIs[i].Item1.Right;
                        else Xr = ROIs[j].Item1.Right;

                        ratio = (float)(Yb - Y) / (float)(Xr - X);
                        if (ratio > filtrationParams.minRatio && ratio < filtrationParams.maxRatio)
                        {
                            additionalROIs.Add(new Tuple<Rectangle, int>(new Rectangle(X, Y, Xr - X, Yb - Y), 4));
                        }
                    }

                }


            }

            foreach (Tuple<Rectangle, int> ROI in additionalROIs) ROIs.Add(ROI);
        }

        private int[] expandScope(int a, int b)
        {
            int[] output = new int[b - a + 1];
            for (int i = 0; i < b - a + 1; i++)
            {
                output[i] = a + i;
            }
            return output;
        }

        private void checkConvergenceOfDetection()
        {
            float firstDetectedPedArea, secondDetectedPedArea, areaCoeff, intersectionCoeff;
            Rectangle intersecion;
            bool[] toReduce = new bool[ROIs.Count];
            for (int i = 0; i < ROIs.Count; i++) toReduce[i] = false;

            //Sprawdzanie czy prostokąty sie pokrywają i w jakim stopniu
            for (int i = 0; i < ROIs.Count; i++)
            {
                if (ROIs[i].Item2 == 1) // pierwszy zbior
                {
                    for (int j = 0; j < ROIs.Count; j++)
                    {
                        if (ROIs[j].Item2 == 2) // drugi zbior
                        {
                            //Sprawdzić, czy się w ogóle nakładają
                            if (ROIs[i].Item1.IntersectsWith(ROIs[j].Item1))
                            {
                                firstDetectedPedArea = ROIs[i].Item1.Width * ROIs[i].Item1.Height;
                                secondDetectedPedArea = ROIs[j].Item1.Width * ROIs[j].Item1.Height;
                                areaCoeff = secondDetectedPedArea / firstDetectedPedArea;

                                //Warunek na podobną powierzchnię i połorzenie
                                if (areaCoeff > filtrationParams.similatiry && areaCoeff < 2 - filtrationParams.similatiry)
                                {
                                    intersecion = Rectangle.Intersect(ROIs[i].Item1, ROIs[j].Item1);

                                    intersectionCoeff = ((float)(intersecion.Width * intersecion.Height)) / firstDetectedPedArea;
                                    if (intersectionCoeff > filtrationParams.similatiry) toReduce[j] = true;
                                }
                            }

                        }
                    }
                }

            }

            for (int i = ROIs.Count - 1; i >= 0; i--)
            {
                if (toReduce[i]) ROIs.RemoveAt(i);
            }
        }

        private void checkConvergenceOfDetectionForEachElement()
        {
            float firstDetectedPedArea, secondDetectedPedArea, areaCoeff, intersectionCoeff;
            Rectangle intersecion;
            bool[] toReduce = new bool[ROIs.Count];
            for (int i = 0; i < ROIs.Count; i++) toReduce[i] = false;

            //Sprawdzanie czy prostokąty sie pokrywają i w jakim stopniu
            for (int i = 0; i < ROIs.Count; i++)
            {
                for (int j = i + 1; j < ROIs.Count; j++)
                {
                    //Sprawdzić, czy się w ogóle nakładają
                    if (ROIs[i].Item1.IntersectsWith(ROIs[j].Item1))
                    {
                        firstDetectedPedArea = ROIs[i].Item1.Width * ROIs[i].Item1.Height;
                        secondDetectedPedArea = ROIs[j].Item1.Width * ROIs[j].Item1.Height;
                        areaCoeff = secondDetectedPedArea / firstDetectedPedArea;

                        //Warunek na podobną powierzchnię i połorzenie
                        if (areaCoeff > filtrationParams.similatiry && areaCoeff < 2 - filtrationParams.similatiry)
                        {
                            intersecion = Rectangle.Intersect(ROIs[i].Item1, ROIs[j].Item1);

                            if (firstDetectedPedArea > secondDetectedPedArea)
                                intersectionCoeff = (((float)intersecion.Width * (float)intersecion.Height)) / (float)firstDetectedPedArea;
                            else
                                intersectionCoeff = (((float)intersecion.Width * (float)intersecion.Height)) / (float)secondDetectedPedArea;
                            if (intersectionCoeff > filtrationParams.similatiry) toReduce[j] = true;
                        }
                    }

                }

            }

            for (int i = ROIs.Count - 1; i >= 0; i--)
            {
                if (toReduce[i]) ROIs.RemoveAt(i);
            }
        }

        private void initialBlobSelection()
        {
            int boxArea;
            float ratio, fulfillment;
            double magnitude;
            shouldBeRemoved = new bool[bbox.Count];
            for (int i = 0; i < bbox.Count; i++) shouldBeRemoved[i] = false;

            for (uint i = 1; i < bbox.Count + 1; i++)
            {
                boxArea = bbox[i].BoundingBox.Width * bbox[i].BoundingBox.Height;
                if (bbox[i].BoundingBox.Y > 300) shouldBeRemoved[i - 1] = true;
                else if (boxArea > 35000 || boxArea < filtrationParams.minAreaInit) shouldBeRemoved[i - 1] = true;
                //if (bibek.Value.Area < bibek.Value.BoundingBox.Y * bibek.Value.BoundingBox.Y / 750 - 8) bbox.Remove(bibek);
                else
                {
                    ratio = (float)bbox[i].BoundingBox.Height / (float)bbox[i].BoundingBox.Width;
                    if (ratio < 0.4) shouldBeRemoved[i - 1] = true;

                    else if (boxArea > 150)
                    {
                        magnitude = bbox[i].BlobMoments.N20 + bbox[i].BlobMoments.N02;
                        fulfillment = (float)bbox[i].Area / (float)boxArea;

                        if (magnitude > 3 * filtrationParams.skewnees && bbox[i].BlobMoments.N20 > filtrationParams.skewnees) shouldBeRemoved[i - 1] = true; // warunek na pociągłość
                        else if (bbox[i].BlobMoments.N20 > filtrationParams.skewnees && bbox[i].BlobMoments.N02 > filtrationParams.skewnees)
                        {
                            if (fulfillment < 0.33f) shouldBeRemoved[i - 1] = true;
                        }
                        else if (fulfillment < filtrationParams.fulfilment) shouldBeRemoved[i - 1] = true;

                    }
                }

            }
        }

        private void filterHomogenousRegions(Image<Gray, Byte> inputImage)
        {
            Gray grayResult = new Gray();
            MCvScalar result = new MCvScalar();
            foreach (Tuple<Rectangle, int> ROI in ROIs.ToArray())
            {
                using (Image<Gray, Byte> imageROI = inputImage.Copy(ROI.Item1))
                {
                    imageROI.AvgSdv(out grayResult, out result);
                    if (result.v0 < filtrationParams.homogenousRegionsCoef)
                    {
                        ROIs.Remove(ROI);
                    }
                }
            }
        }

        private void apativelyReduceROInumber(Image<Gray, Byte> inputImage)
        {
            FiltrationParameters tempFiltrationParams = filtrationParams;

            while (ROIs.Count > filtrationParams.maxROIcount)
            {
                filtrationParams.similatiry = filtrationParams.similatiry - 0.05f;
                checkConvergenceOfDetectionForEachElement();

                if (ROIs.Count <= filtrationParams.maxROIcount) break;

                filtrationParams.homogenousRegionsCoef = filtrationParams.homogenousRegionsCoef + 3;
                filtrationParams.maxRatio = filtrationParams.maxRatio - 0.9f;
                filtrationParams.minRatio = filtrationParams.minRatio + 0.3f;
                filtrationParams.minHeightCoef = filtrationParams.minHeightCoef + 0.03f;
                filtrationParams.minArea = filtrationParams.minArea + 15;
                blobsFiltration(inputImage);
            }

            filtrationParams = tempFiltrationParams;
        }

        private void divideWideROIs()
        {
            float ratio;
            int newX, newX2, newWidth, newID;
            for (int i = 0; i < ROIs.Count; i++)
            {
                ratio = (float)ROIs[i].Item1.Height / (float)ROIs[i].Item1.Width;
                if (ratio >= 1.2 && ratio < 1.8)
                {
                    newID = 10 * (ROIs[i].Item1.X + ROIs[i].Item1.Y + ROIs[i].Item1.Width + ROIs[i].Item1.Height);
                    ROIs[i] = new Tuple<Rectangle, int>(ROIs[i].Item1, newID);

                    newWidth = ROIs[i].Item1.Width / 2;
                    newX = ROIs[i].Item1.X + newWidth;

                    ROIs.Add(new Tuple<Rectangle, int>(new Rectangle(ROIs[i].Item1.X, ROIs[i].Item1.Y, newWidth, ROIs[i].Item1.Height), newID + 1));
                    ROIs.Add(new Tuple<Rectangle, int>(new Rectangle(newX, ROIs[i].Item1.Y, newWidth, ROIs[i].Item1.Height), newID + 1));
                }
                else if (ratio > filtrationParams.minRatio && ratio < 1.2)
                {
                    newID = 10 * (ROIs[i].Item1.X + ROIs[i].Item1.Y + ROIs[i].Item1.Width + ROIs[i].Item1.Height);
                    ROIs[i] = new Tuple<Rectangle, int>(ROIs[i].Item1, newID);

                    newWidth = ROIs[i].Item1.Width / 3;
                    newX = ROIs[i].Item1.X + newWidth;
                    newX2 = ROIs[i].Item1.X + 2 * newWidth;

                    ROIs.Add(new Tuple<Rectangle, int>(new Rectangle(ROIs[i].Item1.X, ROIs[i].Item1.Y, newWidth, ROIs[i].Item1.Height), newID + 1));
                    ROIs.Add(new Tuple<Rectangle, int>(new Rectangle(newX, ROIs[i].Item1.Y, newWidth, ROIs[i].Item1.Height), newID + 1));
                    ROIs.Add(new Tuple<Rectangle, int>(new Rectangle(newX2, ROIs[i].Item1.Y, newWidth, ROIs[i].Item1.Height), newID + 1));
                }
            }
        }

        public int getMinPedestrianArea()
        {
            return minROIarea;
        }

        public bool getReducedROIs()
        {
            return areROIsReduced;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public int getMaxROIcount()
        {
            return filtrationParams.maxROIcount;
        }

        public Image<Gray, Byte> getFirstThresholdedImage()
        {
            return firstThresholdedImage;
        }
        public Image<Gray, Byte> getSecondThresholdedImage()
        {
            return secondThresholdedImage;
        }
        public Image<Gray, Byte> getThirdThresholdedImage()
        {
            return thirdThresholdedImage;
        }

        public FiltrationParameters getFiltrationParams()
        {
            return filtrationParams;
        }
        public Tuple<Rectangle, int>[] getROIsBeforeFiltration()
        {
            return ROIsBeforeFiltration;
        }

        public void setThresholdingMethod(int iThreshMethod)
        {
            thresholdingMethod = iThreshMethod;        }
    }
}
