﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCDS.Classes
{
    abstract class FeatureExtractor
    {
        protected string acronymOfExtractor, fullStringOfParameters;
        protected Size winSize;
        public abstract float[] calculateFeatrureVector(Image<Bgr, Byte> probeImage);
        public abstract string getFullStringOfParameters();
        public abstract Size getResolution();
        public abstract void setResolution(Size newResolution);
        public abstract void setStri(Size newStride);
        public abstract FeatureExtractor deepCopy();
    }
}
