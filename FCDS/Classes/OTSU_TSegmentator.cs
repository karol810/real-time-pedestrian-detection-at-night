﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;

namespace FCDS.Classes
{
    class OTSU_TSegmentator : TSegmentator
    {
        /* parametry algorytmu */
        public OTSU_TSegmentator(FiltrationParameters iFilParams) : base (iFilParams)
        {

        }
        protected override Image<Gray, byte> treshFrame(Image<Gray, byte> sourceImage, int threshold)
        {
            int thres = Otsu_thresholding(sourceImage);
            Image<Gray, byte> thresholdedImage = new Image<Gray, byte>(sourceImage.Size);

            CvInvoke.cvThreshold(
                                  sourceImage
                                , thresholdedImage
                                , thres + threshold, 255
                                , Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY
                                );

            return thresholdedImage;
        }

        private int Otsu_thresholding(Image<Gray, Byte> grayImage)
        {
 
            float[] histData = new float[256];
            using (DenseHistogram Histo = new DenseHistogram(256, new RangeF(0.0f, 255.0f)))
            {
                Histo.Calculate<Byte>(new Image<Gray, byte>[] { grayImage }, false, null);

                
                Histo.MatND.ManagedArray.CopyTo(histData, 0);
            }


            // Total number of pixels
            int total = grayImage.Data.Length;
            double sum = 0;
            for (int t = 0; t < 256; t++) sum += t * histData[t];

            double sumB = 0;
            int wB = 0;
            int wF = 0;

            double varMax = 0;
            int threshold = 0;

            for (int t = 0; t < 256; t++)
            {
                wB += (int)(histData[t]);               // Weight Background
                if (wB == 0) continue;

                wF = total - wB;                 // Weight Foreground
                if (wF == 0) break;

                sumB += (float)(t * histData[t]);

                double mB = sumB / wB;            // Mean Background
                double mF = (sum - sumB) / wF;    // Mean Foreground

                // Calculate Between Class Variance
                double varBetween = (double)wB * (double)wF * (mB - mF) * (mB - mF);

                // Check if new maximum found
                if (varBetween > varMax)
                {
                    varMax = varBetween;
                    threshold = t;
                }
            }
            return threshold;
        }
    }
}
