﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCDS.Classes
{
    class CVC_TDataset : TDataset
    {
        public CVC_TDataset(string path)
            : base(path)
        {
        }

        public override void createDatasetFolders(int xResolution = 64, int yResolution = 128)
        {
            base.createDatasetFolders(xResolution, yResolution);

            calculatePositiveSubDataset("Train", xResolution, yResolution);
            calculatePositiveSubDataset("Test", xResolution, yResolution);

            #region Create negative train dataset
            int counter;
            string destination, SourceDir, srcFile, nameFile;
            Image<Bgr, Byte> sourceImage, cutedImage;
            string[] Files;

            destination = "Train\\FramesNeg";
            SourceDir = pathToDatabase + destination;

            // pobranie listy plików z katalogu
            Files = Directory.GetFileSystemEntries(SourceDir);

            counter = 1;
            // pętla po wszystkich plikach
            for (int i = 0; i < Files.Length; i++)
            {
                // pobranie pełnej nazwy pliku
                srcFile = Path.GetFullPath(Files[i]);
                try
                {
                    // wczytanie obrazu
                    using (sourceImage = new Image<Bgr, byte>(srcFile))
                    {
                        //Przycinanie obrazków i obliczanie HOG
                        for (int k = 0; k < 3; k++)
                        {
                            for (int l = 0; l < 10; l++)
                            {
                                cutedImage = sourceImage.Copy(new Rectangle(l * 64, k * 128, 64, 128));
                                cutedImage.Save(pathToDatabase + "\\PreparedDataset\\" + "Train\\Neg\\" + counter + ".png");
                                counter++;
                                cutedImage.Dispose();
                                GC.SuppressFinalize(cutedImage);
                            }
                        }
                    }
                }
                catch
                {

                }

            }

            #endregion

            #region Create negative test dataset
            int corX, corY, width, height, diff;
            string[] coordinates;
            string textLine;
            bool intersect;
            List<Rectangle> pedestrianRectangle = new List<Rectangle>();

            destination = "Test\\FramesPos";
            SourceDir = pathToDatabase + destination;

            // pobranie listy plików z katalogu
            Files = Directory.GetFileSystemEntries(SourceDir);

            counter = 1;
            // pętla po wszystkich plikach
            for (int i = 0; i < Files.Length; i++)
            {
                pedestrianRectangle.Clear();
                // pobranie pełnej nazwy pliku
                srcFile = Path.GetFullPath(Files[i]);
                nameFile = Path.GetFileNameWithoutExtension(Files[i]);

                using (StreamReader sr = new StreamReader(pathToDatabase + "Test\\Annotations\\" + nameFile + ".txt"))
                {
                    while (sr.EndOfStream == false)
                    {
                        textLine = sr.ReadLine();
                        coordinates = textLine.Split(new Char[] { ' ' });
                        width = Int32.Parse(coordinates[2]);
                        height = Int32.Parse(coordinates[3]);
                        corX = Int32.Parse(coordinates[0]) - width / 2;
                        corY = Int32.Parse(coordinates[1]) - height / 2;

                        if (corX < 0) corX = 0;
                        if (width > 639) width = 639;
                        if (height > 479) height = 479;
                        if (corX < 0) corX = 0;
                        if (corY < 0) corY = 0;
                        if (corX + width > 640)
                        {
                            diff = corX + width - 640;
                            corX = corX - diff;
                        }
                        if (corY + height > 480)
                        {
                            diff = corY + height - 480;
                            corY = corY - diff;
                        }
                        pedestrianRectangle.Add(new System.Drawing.Rectangle(corX
                                                                     , corY
                                                                     , width
                                                                     , height));
                    }

                }

                try
                {
                    // wczytanie obrazu
                    using (sourceImage = new Image<Bgr, byte>(srcFile))
                    {
                        //Przycinanie obrazków i obliczanie HOG
                        for (int k = 0; k < 3; k++)
                        {
                            for (int l = 0; l < 10; l++)
                            {
                                intersect = false;
                                foreach (Rectangle pedestrian in pedestrianRectangle)
                                {
                                    if (pedestrian.IntersectsWith(new Rectangle(l * 64, k * 128, 64, 128)))
                                        intersect = true;
                                }
                                if (!intersect)
                                {
                                    cutedImage = sourceImage.Copy(new Rectangle(l * 64, k * 128, 64, 128));
                                    cutedImage.Save(pathToDatabase + "\\PreparedDataset\\" + "Test\\Neg\\" + counter + ".png");
                                    cutedImage.Dispose();
                                    GC.SuppressFinalize(cutedImage);
                                    counter++;

                                }

                            }
                        }
                    }
                }
                catch
                {

                }

            }

            #endregion

        }

        private void calculatePositiveSubDataset(string typeOfSet, int xRes, int yRes)
        {
            Image<Bgr, Byte> sourceImage, cutedImage;
            int counter, corX, corY, width, height, diff;
            String destination;
            string[] Files, coordinates;
            string srcFile, SourceDir, nameFile, textLine;

            destination = typeOfSet + "\\FramesPos";
            SourceDir = pathToDatabase + destination;

            // pobranie listy plików z katalogu
            Files = Directory.GetFileSystemEntries(SourceDir);

            counter = 1;
            // pętla po wszystkich plikach
            for (int i = 0; i < Files.Length; i++)
            {
                // pobranie pełnej nazwy pliku
                srcFile = Path.GetFullPath(Files[i]);
                nameFile = Path.GetFileNameWithoutExtension(Files[i]);

                // wczytanie obrazu 
                try
                {
                    using (sourceImage = new Image<Bgr, Byte>(srcFile))
                    {
                        // wczytanie opisu
                        using (StreamReader sr = new StreamReader(pathToDatabase + typeOfSet + "\\Annotations\\" + nameFile + ".txt"))
                        {
                            while (sr.EndOfStream == false)
                            {
                                textLine = sr.ReadLine();
                                coordinates = textLine.Split(new Char[] { ' ' });
                                width = Int32.Parse(coordinates[2]);
                                height = Int32.Parse(coordinates[3]);
                                corX = Int32.Parse(coordinates[0]) - width / 2;
                                corY = Int32.Parse(coordinates[1]) - height / 2;

                                if (corX < 0) corX = 0;
                                if (width > 639) width = 639;
                                if (height > 479) height = 479;
                                if (corX < 0) corX = 0;
                                if (corY < 0) corY = 0;
                                if (corX + width > 640)
                                {
                                    diff = corX + width - 640;
                                    corX = corX - diff;
                                }
                                if (corY + height > 480)
                                {
                                    diff = corY + height - 480;
                                    corY = corY - diff;
                                }

                                cutedImage = sourceImage.Copy(new System.Drawing.Rectangle(corX
                                                                                          , corY
                                                                                          , width
                                                                                          , height));
                                cutedImage.Save(pathToDatabase + "\\PreparedDataset\\" + typeOfSet + "\\Pos\\" + counter + ".bmp");
                                counter++;
                                cutedImage.Dispose();
                                GC.SuppressFinalize(cutedImage);
                            }
                        }
                    }
                }
                catch
                {

                }


            }
        }

        public void calculateDatabaseHeightHistogram()
        {
            int height, error;
            int[] heightHistogram = new int[480];
            string[] testFiles, trainFiles, coordinates;
            string SourceTrainDir, SourceTestDir, nameFile, textLine;
            String destinationTrain, destinationTest;

            error = 0;

            destinationTrain = "Train\\FramesPos";
            destinationTest = "Test\\FramesPos";

            SourceTrainDir = pathToDatabase + destinationTrain;
            SourceTestDir = pathToDatabase + destinationTest;

            // pobranie listy plików z katalogu
            trainFiles = Directory.GetFileSystemEntries(SourceTrainDir);
            testFiles = Directory.GetFileSystemEntries(SourceTestDir);

            // pętla po wszystkich plikach treningowych
            for (int i = 0; i < trainFiles.Length; i++)
            {
                // pobranie pełnej nazwy pliku
                nameFile = Path.GetFileNameWithoutExtension(trainFiles[i]);

                // wczytanie obrazu 
                try
                {
                    // wczytanie treningowego opisu
                    using (StreamReader sr = new StreamReader(pathToDatabase + "Train" + "\\Annotations\\" + nameFile + ".txt"))
                    {
                        while (sr.EndOfStream == false)
                        {
                            textLine = sr.ReadLine();
                            coordinates = textLine.Split(new Char[] { ' ' });
                            height = Int32.Parse(coordinates[3]);
                            heightHistogram[height - 1]++;
                        }
                    }
                }
                catch
                {
                    error++;
                }
            }

            // pętla po wszystkich plikach testowych
            for (int i = 0; i < testFiles.Length; i++)
            {
                // pobranie pełnej nazwy pliku
                nameFile = Path.GetFileNameWithoutExtension(testFiles[i]);

                // wczytanie obrazu 
                try
                {
                    // wczytanie treningowego opisu
                    using (StreamReader sr = new StreamReader(pathToDatabase + "Test" + "\\Annotations\\" + nameFile + ".txt"))
                    {
                        while (sr.EndOfStream == false)
                        {
                            textLine = sr.ReadLine();
                            coordinates = textLine.Split(new Char[] { ' ' });
                            height = Int32.Parse(coordinates[3]);
                            heightHistogram[height - 1]++;
                        }
                    }
                }
                catch
                {
                    error++;
                }
            }

            // Write the string array to a new file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(pathToDatabase + "heightHistogram.txt"))
            {
                foreach (int line in heightHistogram)
                    outputFile.WriteLine(line);
            }

        }

        public override string calculatePositiveOriginalTestDataset()
        {
            Image<Bgr, Byte> sourceImage, cutedImage;
            string returnPath;
            int counter, corX, corY, width, height, diff;
            String destination;
            string[] Files, coordinates;
            string srcFile, SourceDir, nameFile, textLine;

            returnPath = pathToDatabase + "\\PreparedDataset\\Test\\Pos\\Native\\";
            destination = "Test" + "\\FramesPos";
            SourceDir = pathToDatabase + destination;

            // pobranie listy plików z katalogu
            Files = Directory.GetFileSystemEntries(SourceDir);

            counter = 1;
            // pętla po wszystkich plikach
            for (int i = 0; i < Files.Length; i++)
            {
                // pobranie pełnej nazwy pliku
                srcFile = Path.GetFullPath(Files[i]);
                nameFile = Path.GetFileNameWithoutExtension(Files[i]);

                // wczytanie obrazu 
                try
                {
                    using (sourceImage = new Image<Bgr, Byte>(srcFile))
                    {
                        // wczytanie opisu
                        using (StreamReader sr = new StreamReader(pathToDatabase + "Test" + "\\Annotations\\" + nameFile + ".txt"))
                        {
                            while (sr.EndOfStream == false)
                            {
                                textLine = sr.ReadLine();
                                coordinates = textLine.Split(new Char[] { ' ' });
                                width = Int32.Parse(coordinates[2]);
                                height = Int32.Parse(coordinates[3]);
                                corX = Int32.Parse(coordinates[0]) - width / 2;
                                corY = Int32.Parse(coordinates[1]) - height / 2;

                                if (corX < 0) corX = 0;
                                if (width > 639) width = 639;
                                if (height > 479) height = 479;
                                if (corX < 0) corX = 0;
                                if (corY < 0) corY = 0;
                                if (corX + width > 640)
                                {
                                    diff = corX + width - 640;
                                    corX = corX - diff;
                                }
                                if (corY + height > 480)
                                {
                                    diff = corY + height - 480;
                                    corY = corY - diff;
                                }

                                cutedImage = sourceImage.Copy(new System.Drawing.Rectangle(corX
                                                                                          , corY
                                                                                          , width
                                                                                          , height));
                                cutedImage.Save(returnPath + counter + ".bmp");
                                counter++;
                                cutedImage.Dispose();
                                GC.SuppressFinalize(cutedImage);
                            }
                        }
                    }
                }
                catch
                {

                }


            }
            return returnPath;
        }

        public override List<Rectangle>[] getTestFramesAnnotations()
        {
            List<Rectangle>[] resultingList;
            resultingList = new List<Rectangle>[22];

            return resultingList;
        }

        public override List<Rectangle>[] getTrainFramesAnnotations()
        {
            List<Rectangle>[] resultingList;
            resultingList = new List<Rectangle>[22];

            return resultingList;
        }
    }
}
