﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.ML.Structure;
using Emgu.CV.Structure;
using System;
using System.IO;

namespace FCDS.Classes
{
    class CNN_BOOST_Classifier : Classifier
    {
        BOOST_Classifier AdaBoostClassifier;
        CNN_Classifier CNNClassifier;

        public CNN_BOOST_Classifier(MCvBoostParams boostParam, int width, int height)
        {
            AdaBoostClassifier = new BOOST_Classifier(boostParam);
            CNNClassifier = new CNN_Classifier(width, height);
            acronym = "CNN_ECVBOOST";
        }

        public override double predictRespone(float[] sampleToTest)
        {
            //get preditions from both classifiers
            float[] acfFeature = new float[sampleToTest.Length-1];
            Array.Copy(sampleToTest, 1, acfFeature, 0, sampleToTest.Length - 1);
            double adaResponse = AdaBoostClassifier.predictRespone(acfFeature);
            double cnnResponse = sampleToTest[0];

            //return average prediction
            double finalPrediction = (adaResponse + cnnResponse) / 2;
            return finalPrediction;
        }

        public override void saveToFile(string pathToWorkDirrectory, string acronymOfExtractor)
        {
            // empty
        }
        public override void train(float[][] trainPosFeatures, float[][] trainNegFeatures)
        {
            AdaBoostClassifier.train(trainPosFeatures, trainNegFeatures);
        }

        public override void loadFromFile(string pathToClassifier)
        {
            AdaBoostClassifier.loadFromFile(pathToClassifier);
        }

        public override Classifier deepCopy()
        {
            Classifier temporary = AdaBoostClassifier.deepCopy();
            return temporary;
        }

        public override void clearClassifier()
        {
            AdaBoostClassifier.clearClassifier();        

        }
    }
}
