﻿using Emgu.CV;
using System;
using System.Drawing;
using Keras.Layers;
using Keras.Models;
using Keras.Optimizers;
using Numpy;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Python.Runtime;

namespace FCDS.Classes
{
    class CNNFeatureExtractor : FeatureExtractor
    {
        Size blockStride;
        int width, height;
        Sequential model;
        string pathToModel;

        public CNNFeatureExtractor(int imgWidth, int imgHeight, string pathToModelWeights)
        {
            width = imgWidth;
            height = imgHeight;
            acronymOfExtractor = "CNN";
            winSize.Width = width;
            winSize.Height = height;
            pathToModel = pathToModelWeights;

            model = new Sequential();

            model.Add(new Conv2D(48, new Tuple<int, int>(7, 7), padding: "same", input_shape: new Keras.Shape(new int[] { width, height, 1 }), activation: "relu"));
            model.Add(new MaxPooling2D(pool_size: new Tuple<int, int>(2, 2)));
            model.Add(new BatchNormalization());

            model.Add(new Conv2D(128, new Tuple<int, int>(5, 5), padding: "same", activation: "relu"));
            model.Add(new MaxPooling2D(pool_size: new Tuple<int, int>(2, 2)));
            model.Add(new BatchNormalization());

            model.Add(new Conv2D(192, new Tuple<int, int>(3, 3), padding: "same", activation: "relu"));

            model.Add(new Conv2D(192, new Tuple<int, int>(3, 3), padding: "same", activation: "relu"));

            model.Add(new Conv2D(128, new Tuple<int, int>(3, 3), padding: "same", activation: "relu"));
            model.Add(new MaxPooling2D(pool_size: new Tuple<int, int>(2, 2)));

            model.Add(new Flatten());
            model.Add(new Dense(2048, activation: "relu"));
            model.Add(new Dropout(0.5));

            model.Add(new Dense(2048, activation: "relu"));
            model.Add(new Dropout(0.5));

            model.Add(new Dense(1));
            model.Add(new Activation("linear"));

            model.Compile(loss: "binary_crossentropy",
                        optimizer: new RMSprop(lr: 0.0001f),
                        metrics: (new[] { "accuracy" }));
            
            model.LoadWeight(pathToModelWeights);
        }

        public override float[] calculateFeatrureVector(Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> probeImage)
        {
            var imageToPredict = np.empty(1, width, height, 1);
            imageToPredict = imageToPredict.astype(np.float32);

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    imageToPredict[0,j,i,0] = (Numpy.NDarray)(probeImage[i, j].Green/255);
                }
            }

            var responseCNN = model.Predict(imageToPredict, 1);
            float[] returnValue = new float[1];
            returnValue[0] = (float)responseCNN;
            returnValue[0] = normalizeRespone(returnValue[0]);
            return returnValue;
            
        }

        public double[] calculateMultipleFeatrureVectors(Image<Emgu.CV.Structure.Bgr, byte>[] samplesList)
        {
            var imageToPredict = np.empty(samplesList.Length, width, height, 1);
            imageToPredict = imageToPredict.astype(np.float32);
            for (int count = 0; count < samplesList.Length; count++)
            {
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        imageToPredict[count, j, i, 0] = (Numpy.NDarray)(samplesList[count][i,j].Green/255);
                    }
                }
            }

            var responseCNN = model.Predict(imageToPredict, samplesList.Length);
            double[] returnValues = new double[samplesList.Length];
            for (int i = 0; i < samplesList.Length; i++)
            {
                float response = (float)responseCNN[i];
                returnValues[i] = normalizeRespone(response);
            }
            return returnValues;

        }

        private float normalizeRespone(float response)
        {
            float normalizedResponse = (response + 100) / 200;
            if (normalizedResponse < 0) normalizedResponse = 0;
            if (normalizedResponse > 1) normalizedResponse = 1;
            return normalizedResponse;
        }

        public override string getFullStringOfParameters()
        {
            fullStringOfParameters = acronymOfExtractor;
            return fullStringOfParameters;
        }

        public override Size getResolution()
        {
            return winSize;
        }

        public override void setResolution(Size newResolution)
        {
            winSize = newResolution;
        }

        public override void setStri(Size newStride)
        {
            blockStride = newStride;
        }

        public override FeatureExtractor deepCopy()
        {
            FeatureExtractor temporary = new CNNFeatureExtractor(this.width, this.height, this.pathToModel);
            return temporary;
        }
    }
}
