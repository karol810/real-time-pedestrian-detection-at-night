﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCDS.Classes
{
    abstract class Classifier
    {
        protected string acronym;

        public string Acronym
        {
            get
            {
                return acronym;
            }
        }

        public abstract void train(float[][] trainPosFeatures, float[][] trainNegFeatures);
        public abstract double predictRespone(float[] sampleToTest);
        public abstract void saveToFile(string pathToWorkDirrectory, string acronymOfExtractor);
        public abstract void loadFromFile(string pathToClassifier);
        public abstract Classifier deepCopy();
        public abstract void clearClassifier();
        protected Matrix<float> floatToMatrix(float[][] input)
        {
            Matrix<float> output = new Matrix<float>(input.Length, input[0].Length);
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < input[0].Length; j++)
                {
                    output[i, j] = input[i][j];
                }
            }
            return output;
        }
        protected Matrix<float> prepareTrainData(float[][] firstClassData, float[][] secondClassData)
        {
            Matrix<float> trainData;

            int size = firstClassData.Length + secondClassData.Length;
            int f_size = firstClassData[0].Length;
            int pos_size = firstClassData.Length;
            trainData = new Matrix<float>(size, f_size);

            #region Rzutowanie float[][] na Matrix<float> i tworzenie bazy

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < f_size; j++)
                {
                    if (i < pos_size)
                    {
                        trainData[i, j] = firstClassData[i][j];
                    }
                    else
                    {
                        trainData[i, j] = secondClassData[i - pos_size][j];
                    }
                }
            }

            #endregion

            return trainData;
        }
        protected Matrix<float> prepareTrainClasses(float[][] firstClassData, float[][] secondClassData)
        {
            Matrix<float> trainClasses;

            int size = firstClassData.Length + secondClassData.Length;
            int f_size = firstClassData[0].Length;
            int pos_size = firstClassData.Length;
            trainClasses = new Matrix<float>(size, 1);

            #region Rzutowanie float[][] na Matrix<float> i tworzenie bazy

            for (int i = 0; i < size; i++)
            {
                if (i < pos_size) trainClasses[i, 0] = 1;
                else trainClasses[i, 0] = 0;
            }

            #endregion

            return trainClasses;
        }

    }
}
