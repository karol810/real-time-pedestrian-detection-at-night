﻿using Emgu.CV;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCDS.Classes
{
    class ACF_FeatureExtractor : FeatureExtractor
    {
        private HOGDescriptor HOG_descr;
        Size blockSize, blockStride, cellSize;
        int nbins, derivAperture;
        double winSigma, L2HysThreshold;
        bool gammaCorrection;

        float[,] smoothMatrix = { { 0.25f, 0.5f, 0.25f } };
        float[,] averageMatrix = { {0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f},
                                        {0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f},
                                        {0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f},
                                        {0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f},
                                        {0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f},
                                        {0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f},
                                        {0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f},
                                        {0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f},
                                        {0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f},
                                        {0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f, 0.01f}
                                    };

        public ACF_FeatureExtractor(
                                    Size iwinSize
                                  , Size iblockSize
                                  , Size iblockStride
                                  , Size icellSize
                                  , int inbins
                                  , int iderivAperture = 1
                                  , double iwinSigma = -1
                                  , double iL2HysThreshold = 0.2
                                  , bool igammaCorrection = true
                                  )
        {
            winSize = iwinSize;
            blockSize = iblockSize;
            blockStride = iblockStride;
            cellSize = icellSize;
            nbins = inbins;
            derivAperture = iderivAperture;
            winSigma = iwinSigma;
            L2HysThreshold = iL2HysThreshold;
            gammaCorrection = igammaCorrection;

            acronymOfExtractor = "ACF";
            initializeACFDescriptor();


        }

        private void initializeACFDescriptor()
        {
            if (HOG_descr != null) HOG_descr.Dispose();
            HOG_descr = new HOGDescriptor(
                               winSize
                             , blockSize
                             , blockStride
                             , cellSize
                             , nbins
                             , derivAperture
                             , winSigma
                             , L2HysThreshold
                             , gammaCorrection);
        }

        public override float[] calculateFeatrureVector(Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> probeImage)
        {
            float[] returnFeatureVector;
            float[] HOGfV;
            //probeImage._EqualizeHist();
            int reducedImagePixels = probeImage.Width * probeImage.Height / 16;
            float[] brightnessFV = new float[reducedImagePixels];
            float[] normalizedMagnitudeFV = new float[reducedImagePixels];

            Emgu.CV.Image<Emgu.CV.Structure.Gray, float> floatImage;
            Emgu.CV.Image<Emgu.CV.Structure.Gray, float> dX, dY, magnitude, averageMagnitude, normalizedMagnitude;
            ConvolutionKernelF convKernel = new ConvolutionKernelF(smoothMatrix);
            ConvolutionKernelF avrgKernel = new ConvolutionKernelF(averageMatrix);

            HOGfV = HOG_descr.Compute(probeImage, Size.Empty, Size.Empty, null);

            // perform image smooth with mask [1 2 1]/4
            floatImage = probeImage.Convert<Emgu.CV.Structure.Gray, float>();
            floatImage = floatImage.Convolution(convKernel);

            // Calculate gradients magnitude
            dX = new Image<Emgu.CV.Structure.Gray, float>(floatImage.Size);
            dY = new Image<Emgu.CV.Structure.Gray, float>(floatImage.Size);
            magnitude = new Image<Emgu.CV.Structure.Gray, float>(floatImage.Size);
            averageMagnitude = new Image<Emgu.CV.Structure.Gray, float>(floatImage.Size);
            normalizedMagnitude = new Image<Emgu.CV.Structure.Gray, float>(floatImage.Size);

            CvInvoke.cvSobel(floatImage, dX, 1, 0, 3);
            CvInvoke.cvSobel(floatImage, dY, 0, 1, 3);
            CvInvoke.cvCartToPolar(dX, dY, magnitude, IntPtr.Zero, true);

            // calculate average magnitude for each pixel with 10x10 mask
            averageMagnitude = magnitude.Convolution(avrgKernel);

            // calculate normalized gradnient magnitude
            CvInvoke.cvAddS(averageMagnitude, new Emgu.CV.Structure.MCvScalar(0.005), averageMagnitude, IntPtr.Zero);
            CvInvoke.cvDiv(magnitude, averageMagnitude, normalizedMagnitude, 1);

            // divide normalized magnitude and brightness into blocks 4x4
            int sumBrightness = 0;
            int row, col;
            float sumNormalizedGradients = 0;
            int newWidth = probeImage.Width / 4;
            for (int i = 0; i < reducedImagePixels; i++)
            {
                row = 4* i / probeImage.Width;
                col = 4* (i % newWidth);
                for (int j = 0; j < 4; j++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        sumBrightness = sumBrightness + probeImage.Data[row+j, col+k,0];
                        sumNormalizedGradients = sumNormalizedGradients + normalizedMagnitude.Data[row + j, col + k, 0];
                    }
                }
                brightnessFV[i] = sumBrightness;
                normalizedMagnitudeFV[i] = sumNormalizedGradients;
                sumBrightness = 0;
                sumNormalizedGradients = 0;
            }
            //normalizedMagnitude = normalizedMagnitude.Mul(50);
            //Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> imageToSave = normalizedMagnitude.Convert<Emgu.CV.Structure.Bgr, byte>();
            //probeImage.Save("C:\\Materialy\\TestFolder\\originalImage.png");
            //magnitude.Save("C:\\Materialy\\TestFolder\\magnitude.png");
            //imageToSave.Save("C:\\Materialy\\TestFolder\\normalizedMagnitude.png");

            // perform vector normalization int 0:1 range.
            float maxBrightnesVal = 16*255;
            float maxNormalizedMagnitudeVal = normalizedMagnitudeFV.Max();
            for (int i = 0; i < reducedImagePixels; i++)
            {
                brightnessFV[i] = brightnessFV[i] / maxBrightnesVal;
                normalizedMagnitudeFV[i] = normalizedMagnitudeFV[i] / maxNormalizedMagnitudeVal;
            }

            returnFeatureVector = new float[HOGfV.Length + brightnessFV.Length + normalizedMagnitudeFV.Length];
            HOGfV.CopyTo(returnFeatureVector, 0);
            brightnessFV.CopyTo(returnFeatureVector, HOGfV.Length);
            normalizedMagnitudeFV.CopyTo(returnFeatureVector, HOGfV.Length + brightnessFV.Length);

            floatImage.Dispose();
            dX.Dispose();
            dY.Dispose();
            magnitude.Dispose();
            averageMagnitude.Dispose();
            normalizedMagnitude.Dispose();
            convKernel.Dispose();
            avrgKernel.Dispose();

            GC.SuppressFinalize(floatImage);
            GC.SuppressFinalize(dX);
            GC.SuppressFinalize(dY);
            GC.SuppressFinalize(magnitude);
            GC.SuppressFinalize(averageMagnitude);
            GC.SuppressFinalize(normalizedMagnitude);
            GC.SuppressFinalize(convKernel);
            GC.SuppressFinalize(avrgKernel);
            return returnFeatureVector;
        }

        public override string getFullStringOfParameters()
        {
            fullStringOfParameters =
                        acronymOfExtractor + "_"
                        + winSize.Width + "_"
                        + winSize.Height + "_"
                        + blockSize.Width + "_"
                        + blockSize.Height + "_"
                        + blockStride.Width + "_"
                        + blockStride.Height + "_"
                        + cellSize.Width + "_"
                        + cellSize.Height + "_"
                        + nbins;

            return fullStringOfParameters;
        }

        public override Size getResolution()
        {
            return winSize;
        }

        public override void setResolution(Size newResolution)
        {
            winSize = newResolution;
            initializeACFDescriptor();
        }

        public override void setStri(Size newStride)
        {
            blockStride = newStride;
            initializeACFDescriptor();
        }

        public override FeatureExtractor deepCopy()
        {
            FeatureExtractor temporary = new ACF_FeatureExtractor(this.winSize, this.blockSize, this.blockStride, this.cellSize, this.nbins, this.derivAperture, this.winSigma, this.L2HysThreshold, this.gammaCorrection);
            return temporary;
        }
    }
}
