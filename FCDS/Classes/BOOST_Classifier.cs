﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.ML.Structure;
using Emgu.CV.Structure;
using System;
using System.IO;

namespace FCDS.Classes
{
    class BOOST_Classifier : Classifier
    {
        Boost classifier;
        MCvBoostParams boostParams;
        float[] weakWeights;

        public BOOST_Classifier(MCvBoostParams boostParam)
        {
            boostParams = boostParam;
            acronym = "ECVBOOST";
            weakWeights = new float[boostParam.weakCount];

            classifier = new Boost();
        }

        public override double predictRespone(float[] sampleToTest)
        {
            Matrix<float> matrixSample = new Matrix<float>(sampleToTest);
            Matrix<float> weakResponses = new Matrix<float>(1, boostParams.weakCount);

            weakResponses.Dispose();
            weakResponses = new Matrix<float>(1, boostParams.weakCount);
            float discreteResponse = classifier.Predict(matrixSample, null, weakResponses, MCvSlice.WholeSeq, false);

            // Corrects results by weights
            //for (int i = 0; i < boostParams.weakCount; i++)
            //    weakResponses[0, i] = weakWeights[i] * weakResponses[0, i];

            double summedResponses = summedResponse(weakResponses);
            double response = normalizeResponse(summedResponses);

            matrixSample.Dispose();
            weakResponses.Dispose();

            return response;
        }

        private double summedResponse(Matrix<float> weakResponsesT)
        {
            double sum = CvInvoke.cvSum(weakResponsesT).v0;
            return sum;
        }

        private double normalizeResponse(double summedResponse)
        {
            double normalizedResponse = (summedResponse + 500)/1000;
            if (normalizedResponse < 0) normalizedResponse = 0;
            if (normalizedResponse > 1) normalizedResponse = 1;
            return normalizedResponse;
        }

        public override void saveToFile(string pathToWorkDirrectory, string acronymOfExtractor)
        {
            classifier.Save(pathToWorkDirrectory + Acronym + "_" + acronymOfExtractor);
            using (StreamWriter outputFile = new StreamWriter(pathToWorkDirrectory + Acronym + "_" + acronymOfExtractor + ".txt"))
            {
                for (int i = 0; i < weakWeights.Length; i++) outputFile.WriteLine(weakWeights[i].ToString());
            }
        }
        public override void train(float[][] trainPosFeatures, float[][] trainNegFeatures)
        {
            classifier = new Boost();
            Matrix<float> trainData, trainClasses;
            int var_count = trainPosFeatures[0].Length;
            Matrix<byte> var_type = new Matrix<byte>(var_count + 1, 1);
            var_type.SetValue(0, null);
            var_type[var_count, 0] = (byte)Emgu.CV.ML.MlEnum.VAR_TYPE.CATEGORICAL;

            trainData = prepareTrainData(trainPosFeatures, trainNegFeatures);
            trainClasses = prepareTrainClasses(trainPosFeatures, trainNegFeatures);

            classifier.Train(trainData, Emgu.CV.ML.MlEnum.DATA_LAYOUT_TYPE.ROW_SAMPLE, trainClasses, var_type, null, boostParams, false);

        }

        public override void loadFromFile(string pathToClassifier)
        {
            classifier.Load(pathToClassifier);
            using (StreamReader outputFile = new StreamReader(pathToClassifier + ".txt"))
            {
                for (int i = 0; i < weakWeights.Length; i++) weakWeights[i] = float.Parse(outputFile.ReadLine());
            }
        }    

        public override Classifier deepCopy()
        {
            Classifier temporary = new BOOST_Classifier(this.boostParams);
            return temporary;
        }

        public override void clearClassifier()
        {
            if (classifier != null)
            {
                classifier.Clear();
                classifier.Dispose();
            }
                
        }

        public MCvBoostParams getBoostParams()
        {
            return boostParams;
        }
    }
}
