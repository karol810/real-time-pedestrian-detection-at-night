﻿using Emgu.CV;
using libsvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCDS.Classes
{
    class LIBSVM_Classifier : Classifier
    {
        public LIBSVM_Classifier()
        {
            acronym = "LIBSVM";
        }

        C_SVC classifier;

        public override double predictRespone(float[] sampleToTest)
        {
            int featureSize = sampleToTest.Length;
            Dictionary<int, double> prob;
            svm_node[] temp = new svm_node[featureSize];
            for (int j = 0; j < featureSize; j++) // Save values for each attributes
            {
                var attributeValue = sampleToTest[ j]; // value of the corresponding attribute
                temp[j] = new svm_node() { index = j, value = attributeValue };
            }
            prob = classifier.PredictProbabilities(temp);
            double response = prob[1];
            return response;
        }

        public override void saveToFile(string pathToWorkDirrectory, string acronymOfExtractor)
        {
            classifier.Export(pathToWorkDirrectory + Acronym + "_" + acronymOfExtractor);
        }

        public override void train(float[][] trainPosFeatures, float[][] trainNegFeatures)
        {
            //svm_parameter parameters = new svm_parameter();
            svm_problem trainDataset = prepareTrainDatasets(trainPosFeatures, trainNegFeatures);
            classifier = new C_SVC(trainDataset, KernelHelper.LinearKernel(), 1, 100, true);
            var accuracy = classifier.GetCrossValidationAccuracy(5);

        }

        private svm_problem prepareTrainDatasets(float[][] trainPosFeatures, float[][] trainNegFeatures)
        {
            int size = trainPosFeatures.Length + trainNegFeatures.Length;
            int f_size = trainPosFeatures[0].Length;
            int pos_size = trainPosFeatures.Length;
            
            var vy = new List<double>();
            var vx = new List<svm_node[]>();
            List<svm_node> x;
            double attributeValue = 0;

            for (int i = 0; i < size; i++)
            {
                if (i < pos_size) vy.Add(1);
                else vy.Add(-1);

                x = new List<svm_node>();
                for (int j = 0; j < f_size; j++) // Save values for each attributes
                {
                    if (i < pos_size)
                    {
                        attributeValue = trainPosFeatures[i][j];
                    }
                    else
                    {
                        attributeValue = trainNegFeatures[i - pos_size][j];
                    }
                    x.Add(new svm_node() { index = j, value = attributeValue });
                }
                vx.Add(x.ToArray());
            }
            var data_set = new svm_problem();
            data_set.l = vy.Count;
            data_set.x = vx.ToArray();
            data_set.y = vy.ToArray();

            return data_set;
        }

        public override void loadFromFile(string pathToClassifier)
        {
 
            classifier = new C_SVC(pathToClassifier);
        }

        public override Classifier deepCopy()
        {
            Classifier temporary = new LIBSVM_Classifier();
            return temporary;
        }

        public override void clearClassifier()
        {

        }
    }
}
