﻿using Emgu.CV;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCDS.Classes
{
    class ACF_CNN_FeatureExtractor : FeatureExtractor
    {
        ACF_FeatureExtractor acfExtractor;
        CNNFeatureExtractor cnnExtractor;

        public ACF_CNN_FeatureExtractor(
            ACF_FeatureExtractor iacfExtractor,
            CNNFeatureExtractor icnnExtractor)
        {
            acfExtractor = iacfExtractor;
            cnnExtractor = icnnExtractor;
        }

        public override float[] calculateFeatrureVector(Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> probeImage)
        {
            // get the predicitons
            float[] acfFVector = acfExtractor.calculateFeatrureVector(probeImage);
            float[] cnnFVector = cnnExtractor.calculateFeatrureVector(probeImage);

            // combine final feature vector
            float[] returnFeatureVector = new float[acfFVector.Length + 1];
            returnFeatureVector[0] = cnnFVector[0];
            acfFVector.CopyTo(returnFeatureVector, 1);

            return returnFeatureVector;
        }

        public float[] calculateFeatrureVector(Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> acfProbeImage,
                                               Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> cnnProbeImage)
        {
            // get the predicitons
            float[] acfFVector = acfExtractor.calculateFeatrureVector(acfProbeImage);
            float[] cnnFVector = cnnExtractor.calculateFeatrureVector(cnnProbeImage);

            // combine final feature vector
            float[] returnFeatureVector = new float[acfFVector.Length + 1];
            returnFeatureVector[0] = cnnFVector[0];
            acfFVector.CopyTo(returnFeatureVector, 1);

            return returnFeatureVector;
        }

        public override string getFullStringOfParameters()
        {
            return acfExtractor.getFullStringOfParameters();
        }

        public override Size getResolution()
        {
            return acfExtractor.getResolution();
        }

        public override void setResolution(Size newResolution)
        {
            winSize = newResolution;
        }

        public override void setStri(Size newStride)
        {
        }

        public override FeatureExtractor deepCopy()
        {
            FeatureExtractor temporary = acfExtractor.deepCopy();
            return temporary;
        }
    }
}
