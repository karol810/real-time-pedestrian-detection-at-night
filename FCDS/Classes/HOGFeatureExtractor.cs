﻿using Emgu.CV;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCDS.Classes
{
    class HOGFeatureExtractor : FeatureExtractor
    {
        private HOGDescriptor HOG_descr;
        Size blockSize, blockStride, cellSize;
        int nbins, derivAperture;
        double winSigma, L2HysThreshold;
        bool gammaCorrection;
        
        public HOGFeatureExtractor(
                                    Size iwinSize
                                  , Size iblockSize
                                  , Size iblockStride
                                  , Size icellSize
                                  , int inbins
                                  , int iderivAperture = 1
                                  , double iwinSigma = -1
                                  , double iL2HysThreshold = 0.2
                                  , bool igammaCorrection = true
                                  )
        {
            winSize = iwinSize;
            blockSize = iblockSize;
            blockStride = iblockStride;
            cellSize = icellSize;
            nbins = inbins;
            derivAperture = iderivAperture;
            winSigma = iwinSigma;
            L2HysThreshold = iL2HysThreshold;
            gammaCorrection = igammaCorrection;

            acronymOfExtractor = "HOG";
            initializeHOGDescriptor();       	
        }

        private void initializeHOGDescriptor()
        {
            HOG_descr = new HOGDescriptor(
                               winSize
                             , blockSize
                             , blockStride
                             , cellSize
                             , nbins
                             , derivAperture
                             , winSigma
                             , L2HysThreshold
                             , gammaCorrection);
        }

        public override float[] calculateFeatrureVector(Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> probeImage)
        {
            float[] returnValue;
            returnValue = HOG_descr.Compute(probeImage, Size.Empty, Size.Empty, null);
            return returnValue;
        }

        public override string getFullStringOfParameters()
        {
            fullStringOfParameters =
                        acronymOfExtractor + "_"
                        + winSize.Width + "_"
                        + winSize.Height + "_"
                        + blockSize.Width + "_"
                        + blockSize.Height + "_"
                        + blockStride.Width + "_"
                        + blockStride.Height + "_"
                        + cellSize.Width + "_"
                        + cellSize.Height + "_"
                        + nbins;

            return fullStringOfParameters;
        }

        public override Size getResolution()
        {
            return winSize;
        }

        public override void setResolution(Size newResolution)
        {
            winSize = newResolution;
            initializeHOGDescriptor();
        }

        public override void setStri(Size newStride)
        {
            blockStride = newStride;
            initializeHOGDescriptor();
        }

        public override FeatureExtractor deepCopy()
        {
            FeatureExtractor temporary = new HOGFeatureExtractor(this.winSize, this.blockSize, this.blockStride, this.cellSize, this.nbins, this.derivAperture, this.winSigma, this.L2HysThreshold, this.gammaCorrection);
            return temporary;
        }
    }
}
